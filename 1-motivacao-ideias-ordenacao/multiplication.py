def plus(d, e, b):
    s = d + e
    return s//b, s % b


def times(d, e, b):
    s = d * e
    return s//b, s % b


def intsum(x, y, n, b):
    x = x[::-1]
    y = y[::-1]
    carry = 0
    result = [0] * (n + 1)
    for i in range(n):
        c1, r = plus(x[i], y[i], b)
        c2, r = plus(r, carry, b)
        carry = c1 + c2
        result[i] = r
    result[n] = carry
    return result[::-1]


def complement(x, n, b):
    one_complement = [(b-1-d) for d in x]
    one = [0]*(n-1) + [1]
    return intsum(one_complement, one, n, b)[1:]


def intminus(x, y, n, b):
    c = complement(y, n, b)
    s = intsum(x, c, n, b)
    return s[1:]


def grade_school_multiply(x, y, n, b):
    x = x[::-1]
    y = y[::-1]
    results = [[0]*2*n for i in range(n)]

    for i in range(n):
        carry = 0
        for j in range(n):
            c1, m = times(x[j], y[i], b)
            c2, m = plus(m, carry, b)
            _, carry = plus(c2, c1, b)
            results[i][j + i] = m
        results[i][n+i] = carry
        results[i] = results[i][::-1]

    result = results[0]

    for i in range(1, n):
        result = intsum(result, results[i], 2*n, b)
    return result[1:]


def karatsuba_multiply(x, y, n, base):
    zeroes = 0
    while x[zeroes] == 0 and y[zeroes] == 0:
        zeroes += 1

    x = x[zeroes:]
    y = y[zeroes:]

    n -= zeroes

    if n == 1:
        carry, r = times(x[0], y[0], base)
        return [0] * (2*zeroes) + [carry, r]

    h1 = n//2
    h2 = n - h1

    a = x[0:h1]
    b = x[h1:]
    c = y[0:h1]
    d = y[h1:]

    one = karatsuba_multiply(a, c, h1, base)
    s1 = 2*h1
    two = karatsuba_multiply(b, d, h2, base)
    s2 = 2*h2

    padding = [0]*(h2-h1)
    ab = intsum(padding + a, b, h2, base)
    cd = intsum(padding + c, d, h2, base)
    s_ab = h2+1

    three = karatsuba_multiply(ab, cd, s_ab, base)
    s3 = 2*s_ab

    padding = [0] * (s3-s2)

    four = intminus(three, padding + two, s3, base)

    padding = [0] * (s3-s1)

    four = intminus(four, padding + one, s3, base)

    one += [0]*(2*h2)
    s1 += 2*h2

    four += [0]*h2
    s4 = s3 + h2

    padding = [0] * (s1-s4)

    result = intsum(one, padding + four, s1, base)
    sr = s1 + 1

    padding = [0] * (sr-s2)

    return [0] * (2*zeroes) + intsum(result, padding + two, sr, base)[2:]


def recursive_multiply(x, y, n, base):
    if n == 1:
        carry, r = times(x[0], y[0], base)
        return [carry, r]

    h1 = n//2
    h2 = n - h1

    a = x[0:h1]
    b = x[h1:]
    c = y[0:h1]
    d = y[h1:]

    padding = [0] * (h2-h1)

    axc = recursive_multiply(a, c, h1, base)
    axd = recursive_multiply(padding + a, d, h2, base)
    bxc = recursive_multiply(b, padding + c, h2, base)
    bxd = recursive_multiply(b, d, h2, base)

    axdbxc = intsum(axd, bxc, 2*h2, base)

    axc += [0]*(2*h2)
    axdbxc += [0]*h2

    padding = [0]*(2*n - (3*h2+1))

    result = intsum(axc, padding + axdbxc, 2*n, base)

    padding = [0]*((2*n+1) - (h2+1))

    return intsum(result, padding + bxd, 2*n+1, base)[2:]
