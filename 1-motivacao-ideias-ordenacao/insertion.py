def insertion_sort(l):
    n = len(l)
    for i in range(1, n):
        k = l[i]
        j = i - 1
        while j > -1 and l[j] > k:
            l[j + 1] = l[j]
            j -= 1
        l[j + 1] = k
