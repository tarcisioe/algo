def binary_search_r(l, start, n, k):
    if n == 0:
        return False

    half = n//2
    middle = start + half

    if l[middle] == k:
        return True
    elif k < l[middle]:
        return binary_search_r(l, start, half, k)
    else:
        return binary_search_r(l, middle + 1, n - half - 1, k)


def binary_search_i(l, start, n, k):
    while n > 0:
        half = n//2
        middle = start + half
        if l[middle] == k:
            return True
        elif k < l[middle]:
            n = half
        else:
            start = middle + 1
            n = n - half - 1

    return False
