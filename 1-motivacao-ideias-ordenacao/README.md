Aula 1 - Motivação, Idéias Gerais e Ordenação
=============================================

Introdução e Motivação
----------------------

* Questionamentos aos alunos:
    * O que (e a que nível) conhecem de:
        * Estruturas de dados
            * Listas (contíguas e encadeadas)
            * Pilhas e Filas
            * Árvores de Busca
            * Heaps
            * Tabelas de Hash
        * Análise de Complexidade
            * Não recursiva
            * Recursiva
                * Árvore de Recursão
                * Master Method
                * "Guess-and-Prove"
    * O que esperam estudando isso mais a fundo.

* O que será visto, no mínimo:
    * Ordenação por comparação
    * Métodos de prova
    * Análise de complexidade
    * Dividir e conquistar (divide-and-conquer)
    * Problema do par mais próximo
    * Heaps
    * Árvores binárias balanceadas de busca
    * Tabelas hash
    * Grafos
        * Representações
        * Menor corte
        * Busca (em largura e profundidade)
        * Conectividade (não-direcionada)
        * Ordem topológica
        * Componentes fortemente conexas
        * Menor caminho

Exemplo de Problema: Multiplicação de Inteiros
----------------------------------------------

* Entrada: dois inteiros, x e y, o tamanho n dos inteiros e a base b.
* Saída: um inteiro de tamanho 2n com o resultado.

* Um inteiro será considerado uma sequência de n dígitos.
* As operações disponíveis, além de laços e estruturas de controle são:
    * Somar dois inteiros de um dígito (provê um resultado e um carry),
      ao custo de uma operação.
        * `plus(x, y, b)`
    * Multiplicar dois inteiros de um dígito (provê um resultado e um carry),
      ao custo de uma operação.
        * `times(x, y, b)`
    * Somar e subtrair dois inteiros de n dígitos também é provido e utiliza
      n operações.
        * `intsum(x, y, n, b)` (retorna um inteiro de tamanho n+1)
        * `intminus(x, y, n, b)` (não altera n)


Primeira ideia: Multiplicação de Ensino Fundamental
---------------------------------------------------

* Para cada dígito de y, multiplicar x e somar os resultados deslocados
  (multiplicados pela base).
* Por conveniência, os dígitos serão indexados do menos para o mais
  significativo.

```pseudo
multiply(integer x, integer y, size n, base b):
    reverse x and y
    results = int[n][2*n]
    initialize results with 0s

    for i = 0 to n-1:
        carry = 0
        for j = 0 to n - 1:
            c1, m = times(x[j], y[i], b)
            c2, m = plus(m, carry, b)
            _, carry = plus(c2, c1, b)
            results[i][j + i] = m
        results[i][n+i] = carry
        reverse results[i]

    result = int[2*n]
    sum results columnwise with carries and place into result
```

```
    7654
   *4321
    ----
    7654 >
  15308_ > número de operações proporcional
 22962__ > a n²
30616___ >
--------
33072934
```

* Espaço utilizado também é proporcional a n².
* É possível utilizar menos espaço computando diretamente o array final.

Segunda ideia: Multiplicação de Karatsuba
-----------------------------------------

* Escrever x = 10^(n/2)a + b e y = 10^(n/2)c + d.
* Computar recursivamente: (1) = ac, (2) = bd e (3) = (a+b)\*(c+d)
* Calcular (4) = (3) - (2) - (1)
* Somar (1)\*10^(n) + (4)\*10^(n/2) + (2)

```pseudo
karatsuba(integer x, integer y, size n, base b):
    if x and y have leading zeroes in common, discard them
    recompute n according to leading zeroes that were discarded

    if n == 1:
        carry, r = times(x[0], y[0], base)
        return [carry, r] with left-padding of two times the zeroes removed

    h1 = n//2
    h2 = n - h1

    a = x[0:h1]
    b = x[h1:]
    c = y[0:h1]
    d = y[h1:]

    one = karatsuba_multiply(a, c, h1, base)
    s1 = 2*h1   // multiplication doubles the number of digits
    two = karatsuba_multiply(b, d, h2, base)
    s2 = 2*h2

    // h1 ≤ h2, necessarily, so a might need a left-padding to reach
    // b's number of digits. Same happens between c and d.
    padding = [0]*(h2-h1)

    ab = intsum(padding + a, b, h2, base)
    cd = intsum(padding + c, d, h2, base)
    s_ab = h2+1   //addition adds one digit to the result

    three = karatsuba_multiply(ab, cd, s_ab, base)
    s3 = 2*s_ab

    padding = [0] * (s3-s2)  // s3 ≤ s2, necessarily

    four = intminus(three, padding + two, s3, base)

    padding = [0] * (s3-s1)  // s3 ≤ s1, necessarily

    four = intminus(four, padding + one, s3, base)

    // right 0-padding is equal to multiplying by the base, so do it to
    // one and four accordingly
    one += [0]*(2*h2)
    s1 += 2*h2

    four += [0]*h2
    s4 = s3 + h2

    padding = [0] * (s1-s4)  //s1 ≤ s4, necessarily

    result = intsum(one, padding + four, s1, base)
    sr = s1+1

    padding = [0] * (sr-s2)  //sr ≤ s2, necessarily

    return intsum(result, padding + two, sr, base), with two digits from the
    left removed and twice the number of zeroes removed previously added
    to the left

    // multiplication yields a number with maximum size 2*n
    // and the result is of size 2*n+2, so two digits are necessarily
    // irrelevant
```

```
            (7654 * 4321) = 32680000 + 391800 + 1134 = 33072934
            /     |     \
      (76*43)  (54*21)  (130*64)
                 ...
```

* Não é intuitiva a forma como isso funciona. Será vista a seguir.
* Pergunta em aberto: é melhor este do que o algoritmo anterior?

Terceira ideia: Multiplicação Recursiva
---------------------------------------

* Escreva novamente x = 10^(n/2)a + b e y = 10^(n/2)c + d.
* Então x\*y = 10^(n)\*ac + 10^(n/2)\*(ad + bc) + bd.
* Computar ac, ad, bc e bd recursivamente e avaliar a expressão acima.

* Código semelhante à multiplicação de Karatsuba.
* É possível entender claramente a ideia por trás.

* É notável o fato de que existem quatro recursões, ao contrário de três,
  como no algoritmo anterior. Intuitivamente, é razoável pensar que a
  complexidade do algoritmo é maior, portanto. Isso será verificado adiante.

```pseudo
recursive-multiply(integer x, integer y, size n, base base):
    if n == 1:
        carry, r = times(x[0], y[0], base)
        return [carry, r]

    h1 = n//2
    h2 = n - h1

    a = x[0:h1]
    b = x[h1:]
    c = y[0:h1]
    d = y[h1:]

    padding = [0] * (h2-h1)

    axc = recursive_multiply(a, c, h1, base)
    axd = recursive_multiply(padding + a, d, h2, base)
    bxc = recursive_multiply(b, padding + c, h2, base)
    bxd = recursive_multiply(b, d, h2, base)

    axdbxc = intsum(axd, bxc, 2*h2, base)

    axc += [0]*(2*h2)
    axdbxc += [0]*h2

    padding = [0]*(2*n - (3*h2+1))

    result = intsum(axc, padding + axdbxc, 2*n, base)

    padding = [0]*((2*n+1) - (h2+1))

    return intsum(result, padding + bxd, 2*n+1, base) with two digits
    removed from the left

    // once again, multiplication yields a number with maximum size 2*n
    // and the result is of size 2*n+2, so two digits are necessarily
    // irrelevant
```

```
            (7654 * 4321) = 32680000 + 391800 + 1134 = 33072934
            /   /   \   \
     (76*43)   /  (54*43)(54*21)
      ...  (76*21)      ...
            ...
```


Refinando a Multiplicação Recursiva
-----------------------------------

* Revendo (a+b)\*(c+d):
    * O resultado é ac + ad + bc + bd.
    * O resultado menos ac e bd é (ad + bc), que é o necessário na
      expressão.
    * Esse é conhecido como o Truque de Gauss (Gauss's Trick)

* Perceba que o truque de Gauss é exatamente o que diferencia a
  multiplicação de Karatsuba da versão recursiva intuitiva.

* Pergunta em aberto: em algum sentido algum dos métodos vistos é
  melhor que os outros?

Algoritmos Recursivos e Iterativos
----------------------------------

Busca Binária
-------------

* Entrada: um array ordenado A com n elementos, e uma chave k.
* Saída: um booleano indicando a existência de k em a.

* Nota: a saída também pode ser a posição de k em A se estiver presente,
  ou a posição em que k estaria, estando presente ou não, mais um
  booleano indicando presença.

* Implementação ingênua (busca linear):
    * Olhar cada elemento e comparar com k. Retornar verdadeiro se k for
      encontrado ou falso ao passar do final.
    * O número de operações no pior caso será proporcional a n.
    * É possível fazer melhor que isso, pois o array está ordenado.

* Busca binária:
    * Aproveitar o fato de que o array está ordenado para diminuir o espaço
      de busca a cada falha.
    * Começar no meio e ir para um dos lados conforme o resultado.
    * O número de operações no pior caso é proporcional a log(n) (será usado
      log para log2).
    * Com apenas comparações, é impossível fazer melhor, como será visto adiante.

* Implementação recursiva:
    * Comparar o elemento do meio (chamado m) do array com k. Se for igual,
      retornar verdadeiro. Se não:
        * Se k < m, recursionar no subarray à esquerda de m.
        * Se k > m, recursionar no subarray à direita de m.

```pseudo
binary-search-r(array A, index s, size n, key k):
    if n == 0:
        return False

    half = n/2
    middle = s + half

    if A[middle] == k:
        return True
    else if k < A[middle]:
        return binary-search-r(A, s, half, k)
    else:
        return binary-search-r(A, middle + 1, n - half - 1, k)
```

* Implementação iterativa:
    * Enquanto o tamanho do espaço de busca for maior que zero:
        * Olhar o elemento no meio do espaço de busca (m):
            * Se for igual a k, retornar verdadeiro.
            * Se k < m, transformar a primeira metade do espaço de busca no
              novo espaço de busca.
            * Se k < m, transformar a segunda metade do espaço de busca no
              novo espaço de busca.

```pseudo
binary-search-i(array A, index s, size n, key k):
    while n > 0:
        half = n/2
        middle = s + half
        if A[middle] == k:
            return True
        else if k < A[middle]:
            n = half
        else:
            s = middle + 1
            n = n - half - 1

    return False
```

Ordenação Por Comparação
------------------------

* Entrada: um array A com n elementos, uma função de comparação f
  (serão utilizados inteiros e < para exemplificar).
* Saída: uma permutação ordenada do array.

* O que é "ordenado"?
    * Definição formal:
        * Se n <= 1, o array é considerado trivialmente ordenado.
        * Se n > 1, o array é ordenado se para todos índices i < j < n,
          A[i] <= A[j].

* Primeira ideia: Insertion Sort
    * Manter um subarray ordenado no início do array, inicialmente com um
      elemento.
    * Para cada elemento na parte ainda não ordenada do array, inseri-lo
      no subarray ordenado.
    * Facilmente implementado de forma iterativa:

```pseudo
insertion-sort(array A, size n):
    for i = 1 to n-1:
        k = A[i]
        j = i - 1
        while j > -1 and A[j] > k:
            A[j + 1] = A[j]
            j -= 1
        A[j + 1] = k
```

    * Funciona e é possível provar, como será visto adiante.
    * O número de operações é proporcional a n². É possível fazer melhor
      que isso, conforme será será visto em sequência.

* Segunda ideia: Merge Sort
    * Dividir o array em metades, ordenar as metades e criar uma solução
      a partir disso.
    * Facilmente implementável como um processo recursivo.
    * Precisa-se apenas solucionar o problema de unir as soluções.
    * Solução: Merge

    * Merge
        * Entrada: dois arrays A e B, ordenados, de tamanhos m1 e m2.
        * Saída: um array ordenado composto pelos elementos de a e de b.

        * Para simplificar, será utilizado o mesmo array, com pontos de início
          fim diferentes para denotar os subarrays ordenados.

        * Ideia:
            * Comparar os elementos do início de cada array. Selecionar o menor,
              colocá-lo ao final de um novo array de tamanho n+m e avançar no
              array ao qual pertencia o elemento selecionado.
            * Se um dos arrays acabar, simplesmente colocar o resto do outro
              no final.

        * Complexidade: proporcional a m1 + m2, pois a cada iteração um elemento
          será avançado.

    * Pode-se agora definir a implementação do Merge Sort:
        * Se o tamanho for menor que 2, nada necessita ser feito.
        * Ordenar ambas as metades do array recursivamente.
        * Utilizar o merge para combinar as duas metades.

    * Pode-se ter uma ideia do número de operações intuitivamente,
      mas será visto adiante como calcular a complexidade de maneira
      formal.

```pseudo
merge(array A, index s1, index s2, index end):
    m1 = s2 - s1
    m2 = end - s2
    i = 0
    j = 0
    L = int[m1]
    copy A[s1:s2] to L
    R = int[m2]
    copy A[s2:end] to R
    k = s1
    while i < m1 or j < m2:
        if j > (m2-1) or (i < m1 and L[i] ≤ R[j]):
            A[k] = L[i]
            i += 1
        else:
            A[k] = R[j]
            j += 1
        k += 1
```

```pseudo
merge-sort(array A, index s, index e):
    if e - s < 2:
        return
    middle = s + (e - s)/2
    merge-sort(A, s, middle)
    merge-sort(A, middle, e)
    merge(A, s, middle, e)
```
