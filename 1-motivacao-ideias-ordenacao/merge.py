def merge(A, s1, s2, end):
    n = s2 - s1
    m = end - s2
    i = 0
    j = 0
    L = A[s1:s2]
    R = A[s2:end]
    k = s1
    while i < n or j < m:
        if j > (m-1) or (i < n and L[i] < R[j]):
            A[k] = L[i]
            i += 1
        else:
            A[k] = R[j]
            j += 1
        k += 1

def merge_sort(A, s, e):
    if e - s < 2:
        return
    middle = s + (e - s)//2
    merge_sort(A, s, middle)
    merge_sort(A, middle, e)
    merge(A, s, middle, e)
