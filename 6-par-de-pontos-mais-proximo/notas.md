Aula 6 - Par de Pontos Mais Próximos
====================================

* Nesta aula será mostrado o problema do par de pontos mais próximo,
  um problema com uma solução não trivial, e certamente interessante.


O Problema
----------

* Input: Um conjunto P = {p1, ..., pn} de n pontos distintos em um plano.
* Output: Um par p\*, q\* de pontos distintos que minimizam a distância
          euclidiana d(p, q) sobre p, q ∈ P.


Solução por Força Bruta
-----------------------

* A solução por força bruta é trivialmente implementável e analisável:

```pseudo
 1. closest-pair-brute-force(P, n):
 2.     min = INFINITY
 3.     p*, q* = None, None
 4.     for p = 0 to n-1:
 5.         for q = p+1 to n-1:
 6.             dist = distance(p, q)
 7.             if dist < min:
 8.                 min, p*, q* = dist, P[p], P[q]
 9.
10.     return p*, q*
```

* A invariante mantida é que `min` é a menor distância já vista e p\* e q\*
  são os pontos cuja distância é `min`.

* O tempo de execução é Θ(n²).


Uma Solução Sub-quadrática
--------------------------

* Certamente a solução por força bruta não é o objeto de interesse a
  ser discutido. A questão é: é possível fazer melhor de alguma forma?
  Felizmente, a resposta é "sim".

* Para obter uma intuição inicial sobre como resolver o problema mais
  eficientemente que simples força-bruta, é interessante analisar
  a versão unidimensional do problema.


Versão 1-D do problema
----------------------

* O problema é semelhante em estrutura, porém P agora é simplesmente um
  subconjunto finito dos reais.

* Uma solução por força-bruta se procederia da mesma forma que a solução
  para o problema 2-D. Por este motivo, seu tempo de execução seria também
  Θ(n²).

* Porém, uma solução mais simples é consideravelmente clara: Seja P' uma
  permutação ordenada de P. Assim sendo, para i, j com j > i+1, sabemos que
  P'[i] ≤ P'[i+1] ≤ P[j]. Assim sendo, a distância entre P'[i] e P'[i+1],
  dada por |P'[i] - P'[i+1]̭̭̭| é necessariamente menor ou igual à distância
  entre P'[i] e P'[j], ou seja, é necessário apenas verificar cada par de
  pontos adjacentes em P'.

* Assim sendo, um algoritmo válido para o problema seria:

```
 1. closest-pair-1D(P, n):
 2.     P' = merge-sort(P, 0, n)
 3.     min = INFINITY
 4.     p*, q* = None, None
 5.
 6.     for i = 0 to n-2:
 7.         dist = distance(P'[i], P'[i+1])
 8.         if dist < min:
 9.              min, p*, q* = dist, P'[i], P'[i+1]
10.
11.     return p*, q*
```

* Analisando o tempo de execução:
    ```
    1. Θ(n\*log(n))
    2. Θ(1)
    3. Θ(1)
    4. Θ(1)
    5. 0
    6. Θ(n)
        7. Θ(1)
        8. Θ(1)
            9. Θ(1)
    10. 0
    11. Θ(1)
    ```

* Tem-se então Θ(n\*log(n) + Θ(n) + Θ(1)) = Θ(n\*log(n)). Esta solução então
  é assintoticamente mais eficiente que a solução por força-bruta.


Estendendo a Solução Para 2-D
-----------------------------

* A princípio, a solução Θ(n\*log(n)) para o caso 1-D não traz consigo
  nenhuma resposta direta para o problema 2-D. Tentar aplicar a mesma
  estratégia já cria problemas diretamente:
    * Para gerar algum P', seria interessante ordenar P de acordo com x ou
      y?
    * Pior, ordenar por qualquer uma das coordenadas e depois analisar os
      adjacentes pode não dar resultado válido algum. E infelizmente, este
      é realmente o caso.

* Um contraexemplo muito simples para esta estratégia é o seguinte conjunto
  de pontos:
    * {(1, 8), (8, 1), (7, 14), (14, 7), (6, 6), (9, 9)}
      ![Um contraexemplo](images/counterexample.png)

    * O par de menor distância é (6, 6), (9, 9).

    * Ordenar por x produz [(1, 8), (6, 6), (7, 14), (8, 1), (9, 9), (14, 7)].
      O par mais próximo não está adjacente, logo o resultado correto é
      impossível de ser obtido.

    * Ordenar por y produz [(8, 1), (6, 6), (14, 7), (1, 8), (9, 9), (7, 14)].
      Novamente o par mais próximo não está adjacente.

* Portanto, a solução para 1-D não se traduz diretamente para o caso 2-D.

* Porém, é possível imaginar uma solução melhor para a parte final do algoritmo,
  que é Θ(n). Considerando que o algoritmo completo tem sua complexidade elevada
  a Θ(n\*log(n)) por causa da ordenação, desde que não se extrapole uma
  complexidade de Θ(n\*log(n)) para o resto do algoritmo, é possível se manter
  nessa complexidade.

* Ordenar os pontos se provou útil para o caso 1-D. Apesar de não prover a
  resposta direta para o caso 2-D, é possível que ordenar os pontos ainda
  permita o uso de métodos melhores que força-bruta. De fato, ordenar apenas
  por x ou por y não é o suficiente, mas ordenar duas vezes, uma para cada
  coordenada, formará a base para a solução.

* A solução final apresentada será por dividir-e-conquistar. De início,
  um rascunho da solução:
  ```
  1. closest-pair(P, n):
  2.     Px = merge-sort(P, 0, n)
  3.     Py = merge-sort(P, 0, n, compare-by-y)
  4.     closest-pair-recursive(Px, Py, n)
  ```
    * Aqui `compare-by-y` é uma função a ser utilizada por merge-sort para fazer
      comparações entre os elementos, nesse caso levando y em conta antes de x.


* `closest-pair-recursive` é a parte dividir-e-conquistar. Esta parte do
  algoritmo assume que Px e Py são ordenados por x e por y respectivamente:
  ```
  1. closest-pair-recursive-sketch(Px, Py, n)
  2.     half = n/2
  3.     Pxl, Pxr = Px[0:half], Px[half:n]
  4.     Pyl, Pyr = split-x(Py, Px[half])
  5.     dl, pl, ql = closest-pair-recursive(Pxl, Pyl, half)
  6.     dr, pr, qr = closest-pair-recursive(Pxl, Pyl, n-half)
  7.     ds, ps, qs = closest-split-pair(Px, Py, n)
  8.
  9.     return best of (dl, pl, ql), (dr, pr, qr), (ds, ps, qs)
  ```
    * As definições dos passos são as seguintes:
        * `Px[half]` é um ponto que divide o conjunto de pontos exatamente ao
          meio. Isto é equivalente a traçar uma reta no plano, paralela ao eixo
          y, passando por `Px[half]`, e dividindo o plano em duas partes.
        * `Pxl` e `Pxr` são, claramente, as duas metades do conjunto de pontos,
          ordenadas por x.
        * `split-x` é uma função responsável por dividir o conjunto de pontos
          ordenado por y no meio de acordo com a divisão de x. Isso significa
          que `Pyl` e `Pyr` são permutações de `Pxl` e `Pxr`, respectivamente,
          mas ordenadas por y. Isto pode ser feito em tempo linear.
        * `(pl, ql)` e `(pr, qr)` são os pares de pontos mais próximos em cada
          uma das metades do plano, descontando comparações entre pontos que
          residem em uma metade cada um.
        * `(ps, qs)` é o par de menor distância em que `ps` e `qs` residem em
          metades opostas do plano, ou "par dividido".
        * Por fim, o retorno é o melhor dos pares, o que cobre todas as
          possibilidades.

* Até agora, porém, não é nem um pouco claro como implementar
  `closest-split-pair` de maneira razoável: a única forma aparente até agora é
  por força-bruta, com complexidade Θ(n²), o que é proibitivo, pois pelo
  _Master Method_, a complexidade final seria T(n) = 2\*T(n/2) + Θ(n²),
  ou seja, Θ(n²), o que não traria vantagem alguma ao método (pior,
  inseriria recursão e constantes maiores que o método de força bruta simples).

* Porém, existe um detalhe a favor no algoritmo: no ponto em que `(ps, qs)` é
  computado, `(pl, ql)` e `(pr, qr)` já são conhecidos. Sejam ds, dr e dl as
  distâncias respectivas entre os pares de pontos. Seja `(p*s, q*s)`o par
  dividido de menor distância, e d\*s a distância entre eles. Se d\*s não for
  menor que dr e dl, não é necessário que `(ps, qs)` seja `(p*s, q*s)`, pois ele
  não seria o vencedor de qualquer forma.

* Em outras palavras: `closest-split-pair` só precisa computar o menor par
  dividido se ele possuir distância menor que dl e dr. Caso contrário, o
  resultado de `closest-split-pair` é irrelevante para o resto do algoritmo.

* Pode-se utilizar isto dentro do algoritmo da seguinte forma: após as
  computações recursivas, seja δ o menor dentre dr e dl. Com isso,
  `closest-split-pair` pode utilizar esta informação para computar. Só
  é necessário que seu resultado esteja correto se a distância entre
  ps e qs for menor que δ.

* Existem duas propriedades que são verdadeiras quando ds &lt; δ que serão
  úteis para a implementação de `closest-split-pair`:
    * 1: Sejam p, q ∈ P pontos em metades opostas do plano cuja distância
         ds &lt; δ e x' a maior coordenada x de algum ponto na metade esquerda
         do plano. p, q estão em Sy, que é uma permutação ordenada por y
         dos pontos de p cuja coordenada x se encontra no intervalo
         [x'-δ, x'+δ].
        * Prova: Seja p = (xp, yp), q = (xq, yq), e ds &lt; δ.
                 Como p é um ponto à esquerda e q à direita, xp ≤ x'
                 e xq ≥ x'. Mas |xp - xq| &lt; δ, necessariamente, pois
                 a distância euclidiana entre p e q é menor que δ.
                 Então, seja xp &lt; x'-δ. Sabendo que xq ≥ δ,
                 |xp - xq| seria maior que δ. Isto é uma contradição.
                 A prova para xq é simétrica.

          ![Prova 1](images/proof1.png)

    * 2: p e q estão a no máximo 7 posições de distância em Sy.
        * Prova: Seja p = (xp, yp), q = (xq, yq). Seja um retângulo de
                 altura δ e largura 2δ, centrado em (x', min(yp, yq)).
                 Sejam 8 divisões de tamanho δ/2 deste retângulo.

          ![Prova 2](images/proof2.png)

        * Lema 1: Todo ponto em Sy entre p e q, inclusive, está em
                  alguma das divisões.
            * Prova: Todas as coordenadas y destes pontos tem uma diferença
                     menor que δ. Pela definição de Sy, todos tem coordenadas
                     x entre x'-δ e x'+δ. Q.E.D.

        * Lema 2: Cada uma das divisões possui no máximo um ponto de Sy.
            * Prova: Por contradição. Seja a, b ∈ Sy. Supondo que a e b
                     estejam na mesma divisão. Então:
                1. a, b estão ambos à esquerda ou ambos à direita de x'.
                2. A distância entre a e b é menor ou igual a δ/2 * sqrt(2),
                   o que é menor que delta.
            * 1 e 2 contradizem a definição de δ, pois δ é a menor distância
              entre um par de pontos à esquerda ou à direita.

* Dadas estas propriedades, é necessário apenas montar Sy e para cada ponto
  buscar nos 7 pontos seguintes, o que é implementável em tempo linear.

```
 1. closest-split-pair(Px, Py, n, δ):
 2.     biggest-left = n/2 - 1
 3.     p' = Px[biggest-left]
 4.     x', _ = p'
 5.     Sy = [(x,y) for (x,y) in Py if x'-δ ≤ x ≤ x'+δ]
 6.     best = δ
 7.     best-pair = None
 8.
 9.     for i = 0 to |Sy|-2:
10.         for j = 1 to min(7, |Sy|-i-1):
11.             p, q = Sy[i], Sy[i+j]
12.             dist = distance(p, q)
13.             if dist < best:
14.                 best, best-pair = dist, (p, q)
15.
16.     return best, best-pair
```

* `closest-split-pair` é linear e está correto de acordo com a análise anterior.

* Desta forma, pode-se defir `closest-pair-recursive`:

```
 1. closest-pair-recursive(Px, Py, n)
 2.     half = n/2
 3.     Pxl, Pxr = Px[0:half], Px[half:n]
 4.     Pyl, Pyr = split-x(Py, Px[half])
 5.     dl, pl, ql = closest-pair-recursive(Pxl, Pyl, half)
 6.     dr, pr, qr = closest-pair-recursive(Pxl, Pyl, n-half)
 7.     δ = min(dl, dr)
 8.     ds, ps, qs = closest-split-pair(Px, Py, n, δ)
 9.
10.     return min((dl, pl, ql), (dr, pr, qr), (ds, ps, qs))
```

* `closest-pair-recursive` possui duas recursões sobre subproblemas de tamanho
  n/2 (linhas 5 e 6) e uma operação Θ(n) (linha 7). Todas as outras operações
  são constantes. Assim sendo, a recorrência que define T(n) é:
    * T(n) = 2\*T(n/2) + Θ(n)
        1. a = 2
        2. b = 2
        3. d = 1
    * a = b^d, logo T(n) ∈ Θ(n\*log(n)). Assim sendo, a solução por
      dividir-e-conquistar é assintoticamente mais eficiente que a solução
      por força bruta. Q.E.D.!
