Aula 2 - Métodos de Prova
=========================

Sumário
-------

* O que é importante ao analisar algoritmos?
    * Corretude
        * Em geral é o mais importante. Um algoritmo incorreto, para o
          caso geral, não é útil.
        * A frase acima é propositalmente escrita para excluir alguns
          casos.
        * Corretude envolve tanto a corretude da saída como a garantia
          de parada do algoritmo.
        * Normalmente, o que será mais focado é provar a corretude do
          output. A parada do algoritmo costuma ser clara (novamente,
          nem sempre).

    * Complexidade
        * De tempo.
            * Basicamente, quantas operações básicas o algoritmo necessita,
              em termos do tamanho das entradas.
            * Costuma se dar bastante importância, primariamente.
        * De espaço.
            * Quanto espaço auxiliar é necessário durante a execução do
              algoritmo, em termos do tamanho das entradas.
            * Pode ser altamente limitante se não for tomado cuidado, ou,
              em alguns casos, pode ser o problema principal.

    * Aqui, veremos formas de demonstrar a corretude de algoritmos.

Métodos de Prova
----------------

* Como demonstrar corretude?
    * Provas, de diversas formas.
    * O que faz uma prova?
        * Um argumento lógico correto que demonstra a veracidade de um
          predicado esperado.

* Provas não precisam ser estritamente formalizadas em todos os casos.
    * Uma prova pode ser um argumento em vários níveis de detalhe,
      dependendo do caso. Uma demostração nova, ou em um livro-texto
      costuma ser altamente formalizada e detalhada, enquanto uma
      demonstração para sanidade ou de algo já provado e conhecido pode
      ser menos detalhada.
    * O público que irá ler a prova também influi em como a prova deve
      ser escrita. As provas nesse material terão um nível de detalhe
      médio. Ao demonstrar uma prova para outra pessoa, é possível ir
      até o nível de detalhe necessário para o entendimento.

* Serão utilizados aqui dois métodos de prova para algoritmos:
    * Manutenção de invariante de laço (para processos iterativos).
        * Uma invariante é um predicado lógico que é verdadeiro antes do
          laço e novamente no início de cada iteração.
        * Interessa, obviamente, escolher um predicado que ao final do
          laço seja útil para demonstrar que o resultado obtido é o
          esperado.
        * A demonstração via manutenção de invariante terá, portanto,
          quatro componentes:
            * A invariante: um predicado lógico p.
            * Inicialização: um argumento que demonstre que antes do
                             início do laço, p é verdadeiro.
            * Manutenção: um argumento que demonstre que, se o predicado
                          p é verdadeiro logo antes de uma iteração, ele
                          é verdadeiro logo antes da próxima.
            * Término: Um argumento que demonstre que, após o término do
                       laço, o predicado é útil em demonstrar a corretude
                       do algoritmo.
        * Cada passo é construído com base no anterior. Ao final, se
          obtém um argumento lógico para a corretude do algoritmo.

    * Indução (para processos recursivos).
        * Novamente será declarado um predicado p a ser provado. No
          caso da indução, o predicado geralmente se baseia em um
          argumento que é um inteiro não-negativo, normalmente o tamanho
          da entrada para o algoritmo.
        * O predicado mais comum é:
            * p(n) = o algoritmo produz o resultado correto para uma entrada
              de tamanho n`
        * A seguir, serão definidas três coisas:
            * Passo base: uma demonstração de que para algum k constante, o
              mais baixo possível (geralmente 0 ou 1), p(k) é verdadeiro.
            * Hipótese indutiva: a hipótese indutiva é a seguinte suposição:
              para algum n arbitrário, p(m) é verdadeiro se m &lt; n.
                * Detalhe: isso também é chamado hipótese indutiva forte.

                * A hipótese indutiva fraca é simplesmente: para algum n
                  arbitrário, p(n-1) é verdadeiro. Para demonstrar
                  algoritmos, em geral a hipótese indutiva forte é mais
                  útil.
            * Passo indutivo: o verdadeiro corpo da prova. O passo indutivo
              consiste em demonstrar que, para algum n arbitrário, dada a
              hipótese indutiva, p(n) é verdadeiro. Este será o passo onde
              ocorre a real análise do algoritmo, e será no geral o mais
              trabalhoso.
            * Se chamarmos a hipótese indutiva de h(n), o passo indutivo
              é a prova de que h(n) => p(n). O passo base, por exemplo,
              p(0), faz com que h(0) seja verdadeiro, logo p(1) é
              verdadeiro, o que implica em h(1), provando p(2), e assim por
              diante. Isso completa a prova.

* Linhas simples (que não sejam laços nem chamadas de função) podem ser
  avaliadas diretamente.

* Chamadas a outras funções demandam que a outra função seja provada.

Primeiro Exemplo: Busca Binária Iterativa
-----------------------------------------

* Como processo iterativo, será demonstrada a corretude da busca binária
  pelo método de manutenção de invariante.

* Código:
    * Para melhor descrição dos passos, será utilizado o código com as
      linhas enumeradas.

```pseudo
 1. binary-search-i(array A, index s, size n, key k):
 2.     while n > 0:
 3.         half = n/2
 4.         middle = s + half
 5.         if A[middle] == k:
 6.             return True
 7.         else if k < A[middle]:
 8.             n = half
 9.         else:
10.             s = middle + 1
11.             n = n - half - 1
12.
13.     return False
```

* Predicado: o predicado p(s, n) será o seguinte:
    * p(s, n): Não é possível que k seja encontrado fora do subarray
               de tamanho n iniciado no índice s de A, mas ainda dentro
               do espaço de busca desejado
    * Nota: como o algoritmo aceita s e n como argumentos, é possível
            que a busca não seja feita sobre o array todo desde o
            início. Considera-se então que qualquer elemento do array
            fora do espaço determinado pelos valores iniciais de s e n
            como "fora do espaço de busca desejado".

    ```
    Uma iteração arbitrária:

             ___n____
              s       s+(n-1)
             /       /
    ...|####|________|####|...
       { espaço desejado  }

    # = espaço onde certamente não é possível encontrar k
    . = elementos do array possivelmente não contemplados desde o
        início (s e n não alinham com o array todo no início,
        por exemplo)
    _ = espaço de busca ainda não explorado
    ```

* Inicialização:
    * Antes do início do laço, o espaço de busca denotado por
      s e n é todo o espaço desejado, logo se k puder ser
      encontrado, ele está no subarray de tamanho n
      iniciado em s.

* Manutenção:
    * Seja p(s, n) verdadeiro ao início do laço. Na execução
      da linha 5 do algoritmo, seja A[middle] ≠ k (se não,
      k foi encontrado e o laço acaba).

    * Seja k &lt; A[middle]. Como o array é ordenado, para qualquer
      i ≥ middle, k &lt; A[i]. Logo k não pode estar à esquerda
      de middle. Nesse caso, a execução do algoritmo leva à linha
      8, que define n = half (n/2). Ao início da próxima iteração,
      s se manterá e n = half, ou seja, o subarray de n elementos
      iniciado em s termina precisamente antes de A[middle], pois
      middle é s + half. p, portanto, se mantém verdadeiro.

    * Seja k > A[middle]. Pelo argumento simétrico ao anterior,
      para qualquer i ≤ middle, k > A[i]. Nesse caso, as linhas
      10 e 11 reposicionam s para middle + 1, excluindo middle
      e todos os elementos anteriones a ele, e n para n + half + 1.
      Assim, o subarray de n elementos iniciado em s é formado
      precisamente dos elementos que na iteração anterior sucediam
      A[middle]. p, portanto, se mantém verdadeiro.

* Término:
    * Esteja k dentro do espaço de busca desejado no início do
      algoritmo. Pelo predicado p(s, n), não é possível que k esteja
      fora de A[s:s+n] no início de qualquer iteração. Logo,
      k está obrigatoriamente dentro de A[s:s+n], e n é no mínimo 1. Como n
      decresce em todas as iterações onde o teste na linha 5 falha, ou o
      algoritmo termina quando n = 1 (pois k estará presente e será o único
      elemento, logo será A[middle]), ou termina em alguma iteração anterior.
      Mas para o algoritmo terminar com n > 0, é necessário que k seja
      A[middle], ou seja, k foi corretamente encontrado, e a linha 6 retornará
      verdadeiro.

    * Esteja k ausente do espaço de busca desejado no início do
      algoritmo. Assim sendo, é impossível que a linha 6 execute, pois
      A[middle] não pode ser k. Como n sempre decresce nesse caso,
      n eventualmente será 0, quebrando o laço. Após o laço, o algoritmo
      retorna falso. Q.E.D.

Segundo Exemplo: Busca Binária Recursiva
----------------------------------------

* A corretude da versão recursiva da busca binária será demonstrada por
  indução.

* Código:

```pseudo
 1. binary-search-r(array A, index s, size n, key k):
 2.     if n == 0:
 3.         return False
 4.
 5.     half = n/2
 6.     middle = s + half
 7.
 8.     if A[middle] == k:
 9.         return True
10.     else if k < A[middle]:
11.         return binary-search-r(A, s, half, k)
12.     else:
13.         return binary-search-r(A, middle + 1, n - half - 1, k)
```

* Predicado: o predicado p(n) será o seguinte:
    * p(n): Seja um array A, um índice s e uma chave k. `binary-search-r` indica
            corretamente a presença de k em um subarray de n elementos em A
            iniciado em s.

* Passo base:
    * Seja um array A de tamanho 0. Para qualquer k,
      `binary-search-r`(A, 0, 0, k) retorna falso, o que é correto, pois k não
      pode estar em A. Logo, p(0) é verdadeiro.

    * Seja um array A de tamanho 1. Para qualquer k,
      `binary-search-r`(A, 0, 1, k) retorna verdadeiro se k estiver em a, pois
      middle será calculado como 0 + 1/2 = 0, e se A[0] (o único elemento em A)
      for igual a k, a linha 9 retorna verdadeiro. Se não, a recursão ocorre ou
      com n = half = 0 ou com n = 1 - half - 1 = 0, o que conforme visto acima,
      retorna falso. Logo, p(1) é verdadeiro.

* Hipótese indutiva:
    * Seja n um número inteiro não-negativo. Para qualquer m &lt; n, p(m)
      é verdadeiro.

* Passo indutivo:
    * Seja verdadeira a hipótese indutiva. Seja n um inteiro positivo. Seja
      A um array de tamanho t ≥ n. Seja k uma chave. Seja s um índice no
      array a partir do qual hajam no mínimo n elementos.

    * Seja middle = s + n/2, ou seja, o índice do meio do subarray de A de
      tamanho n iniciado em s.

    * Se A[middle] for igual a k, a linha 9 do algoritmo executa e o algoritmo
      retorna verdadeiro corretamente.

    * Se k &lt; A[middle], k só pode preceder A[middle], logo a busca deve ser
      feita nos n/2 elementos a partir de s. Pela hipótese indutiva,
      `binary-search-r` funciona corretamente nesse caso, pois n/2 &lt; n.

    * Se k > A[middle], k só pode suceder A[middle], logo a busca deve ser
      feita nos n - (n/2) - 1 elementos a partir de middle + 1 (pois middle já
      foi avaliado). `binary-search-r` funciona corretamente nesse caso, pois
      (n - (n/2) - 1) &lt; n.

    * Desta forma, a hipótese indutiva implica que p(n) é verdadeiro. Q.E.D.

Terceiro Exemplo: Insertion Sort
--------------------------------

* Por ser um processo iterativo, será utilizado o método da manutenção de
  invariante.

* Código:

```pseudo
 1. insertion-sort(array A, size n):
 2.     for i = 1 to n-1:
 3.         k = A[i]
 4.         j = i - 1
 5.         while j > -1 and A[j] > k:
 6.             A[j + 1] = A[j]
 7.             j -= 1
 8.         A[j + 1] = k
```

* É importante notar que existem laços aninhados. Por isso, é necessário que se
  demonstre a corretude do laço interno inicialmente.

* Demonstração do laço interior:
    * É necessário definir o que se espera que o laço interior faça. Espera-se
      ao final do laço interior, que o subarray que precede A[j+1] seja um
      subarray ordenado com todos os elementos menores ou iguais a k, e o
      subarray que sucede A[j+1] e termina em i seja um subarray ordenado com
      todos os elementos maiores que k. Também se espera que a união dos dois
      subarrays seja composta pelos elementos previamente em A[0:i]. Desta
      forma, haverá um espaço em A[j+1] onde k pode ser colocado sem quebrar a
      ordenação.

    * O laço interior também presume que, antes de seu início, os elementos
      anteriores a A[i] estão ordenados.

    ```
    Ao final do laço interior:

          j        i
           \        \
    |  ≤ k  | |  > k |.........|
    ```

    * Importante notar que ao final do laço interno, k ainda não está em sua
      posição final. Isso é de responsabilidade do laço externo.

    * Com as definições acima, pode-se utilizar o método da manutenção de
      invariante para demonstrar a corretude do laço interno:
        * Predicado:
            * p(j): Para todo elemento e em A[j+2:i+1], e > k e os elementos
                    de A[j+2:i+1] são iguais aos elementos presentes em
                    A'[i-m:i], onde m é o tamanho de A[j+2:i+1] e A' representa
                    o array antes do laço.

        ```
        Array ao início do laço interno:
                      i
                     /
        |_____|_____| |..................|
                /
              A[i-m:i]



        Array em uma iteração arbitrária:
               j j+1   i
              / /     /
        |   |_|_|_____|.................|
                  /
             A[j+2:i+1], de tamanho m, todos os elementos > k e igual a A[i-m:i]
             no array original
        ```


        * Inicialização:
            * De início, j = i - 1, logo A[j+2:i+1] = A[i+1:i+1], que é vazio,
              portanto p(j) é verdadeiro por vácuo. O mesmo vale para a
              igualdade com os elementos presentes em A[i-m:i] no início do
              laço.

        * Manutenção:
            * Pela condição do laço, A[j] > k. A linha 6 sobrescreve A[j+1] com
              A[j]. Como p(j) inicia verdadeiro no laço, após a linha 6
              A[j+1:i+1] é igual a:
              ```
              [A[j]] + A[j+2:i+1].
              ```
              Isso mantém a primeira
              condição (todos elementos maiores que k).

            * O tamanho de A[j+2:i+1], também denominado m, é igual ao número de
              iterações, pois a cada iteração j decresce por 1 (linha 7), e m
              inicia em zero. Desta forma, Como j inicia em i - 1,
              j = i - 1 - m. Logo, após a linha 6, A[j+1:i+1] é igual a
              ```
              [A'[i-m-1]] + A'[i-m:i]
              ```
              Isso mantém a segunda condição (equivalência ao final do array
              original).

        * Término:
            * Ao final do laço, todos os elementos que sucedem A[j+1] são
              maiores que k, e correspondem aos m últimos elementos de A' Como o
              laço só chega ao fim ao encontrar o início de A ou um elemento
              A[j] ≤ k, todos os elementos que precedem A[j+1] são menores ou
              iguais a k, pois o subarray A[0:i] inicia ordenado, e correspondem
              aos i-m primeiros elementos de A'. Desta forma, o laço interno
              está correto. Q.E.D.

* Com o laço interior verificado, podemos proceder à prova do laço exterior,
  e portanto, do algoritmo.

* Predicado:
    * p(i): A[0:i] é uma permutação ordenada de A'[0:i], onde A' é o array ao
            início do laço.

* Inicialização:
    * A[0:1] contém apenas um elemento, A[0], que é igual a A'[0]. Logo,
      p(1) é verdadeiro.

* Manutenção:
    * A linha 2 guarda o valor de A[i] na variável k. Na sequência, o laço
      interno é executado. Pela prova anterior, ao final do laço interno,
      A[0:j+1] é uma permutação ordenada de A'[0:j+1] onde todos os elementos
      são menores ou iguais a k e A[j+2:i+1] é uma permutação ordenada de
      A'[j+2:i+1] onde todos os elementos são maiores ou iguais a k. A linha 8
      define A[j+1] como k, completando a permutação ordenada de A'[0:i+1],
      mantendo a invariante para o próximo valor de i.

* Término:
    * Como a última iteração tem i = n-1, ao final do laço p(n) é verdadeiro,
      ou seja, A[0:n] é uma permutação ordenada de A'[0:n]. Mas A[0:n]
      é a totalidade de A, logo A é uma permutação ordenada de A. Portanto, o
      algoritmo está correto. Q.E.D.

Quarto Exemplo: Merge Sort
--------------------------

* Por fim, utilizaremos um exemplo onde a prova por indução depende de uma prova
  por manutenção de invariante. É necessário demonstrar a corretude do
  procedimento `merge` antes de demonstrar a corretude de `merge-sort`.

* Código:

```pseudo
 1. merge(array A, index s1, index s2, index end):
 2.     m1 = s2 - s1
 3.     m2 = end - s2
 4.     i = 0
 5.     j = 0
 6.     L = int[m1]
 7.     copy A[s1:s2] to L
 8.     R = int[m2]
 9.     copy A[s2:end] to R
10.     k = s1
11.     while i < m1 or j < m2:
12.         if j > (m2-1) or (i < m1 and L[i] ≤ R[j]):
13.             A[k] = L[i]
14.             i += 1
15.         else:
16.             A[k] = R[j]
17.             j += 1
18.         k += 1
```

* A parte de interesse inicia na linha 10. Previamente a isso, existem apenas
  definições.

* A prova, obviamente, depende do fato de que L e R serão arrays ordenados,
  que correspondem à totalidade de  A'[s1:end], onde A' é o array antes do laço.

* Predicado:
    * p(k, i, j): A[s1:k] contém os k-s1 menores elementos da união de L e R em
                  ordem, e L[i] e R[j] são os menores elementos em L e R,
                  respectivamente, que não fazem parte de A[s1:k]. Por fim,
                  k = s1 + i + j.

* Inicialização:
    * Antes do laço, k = s1, portanto o subarray A[s1:k] é vazio. Por vácuo,
      A[s1:k] contém os k-s1 = 0 elementos na união de L e R, e L[i] e R[j]
      são os menores elementos de L e R, respectivamente, que não fazem parte de
      A[s1:k], pois L e R estão ordenados. Por fim, s1 = s1 + 0 + 0.
      Logo, p(s1, 0, 0) é verdadeiro.

* Manutenção:
    * Seja i &lt; m1 e j &lt; m2:
        * Seja L[i] ≤ R[j]. Então L[i] é o menor dos elementos de L e R que não
          pertence a A[s1:k]. A linha 12 executa, copiando L[i] para A[k],
          mantendo ordem, e a linha 13 mais a linha 17 mantém a invariante,
          incrementando i e k.

        * Seja L[i] > R[j]. Por simetria com o argumento anterior, as linhas 16,
          16 e 17 mantém a invariante, incrementando j e k.

    * Seja i ≥ m1 e j &lt; m2:
        * L já foi inteiramente copiado. A condição na linha 11 falha, e as
          linhas 15, 16 e 17 mantém a invariante.

    * Seja i &lt; m1 e j ≥ m2:
        * R já foi inteiramente copiado. A condição na linha 11 é verdadeira,
          e as linhas 12, 13 e 17 mantém a invariante.

* Término:
    * O laço só termina quando i e j ultrapassaram o limite do tamanho de L e R,
      ou seja, pelo predicado, todos os elementos em L e R foram copiados para
      A[s1:k]. Como é possível ver pela manutenção, i e j nunca ultrapassam m1 e
      m2, ou seja, k = s1 + i + j = s1 + m1 + m2 = s1 + end - s1 + s2 - s1 = end.
      Logo, A[s1:k] é uma permutação ordenada de A'[s1:end]. O algoritmo está,
      então, correto. Q.E.D.

* Com a corretude de `merge` em mãos, é possível provar `merge-sort` via
  indução.

* Código:

```pseudo
 1. merge-sort(array A, index s, index e):
 2.     if e - s < 2:
 3.         return
 4.     middle = s + (e - s)/2
 5.     merge-sort(A, s, middle)
 6.     merge-sort(A, middle, e)
 7.     merge(A, s, middle, e)
```

* Predicado:
    * P(n): Seja A[s:e] um subarray de tamanho n. `merge-sort(A, s, e)` ordena
            A[s:e].

* Passo base:
    * Seja A[s:e] um subarray de tamanho 0 ou 1. Pela definição de ordenação,
      A[s:e] está ordenado. As linhas 2 e 3 de `merge-sort` executam
      (e - s = 0 &lt; 2 ou e - s = 1 &lt; 2), o algoritmo retorna e A[s:e]
      continua ordenado. Logo, p(0) e p(1) são verdadeiros.

* Hipótese indutiva:
    * Seja n um número inteiro não-negativo. Para qualquer m &lt; n, p(m)
      é verdadeiro.

* Passo indutivo:
    * Seja A[s:e] um array de tamanho n > 1. A linha 4 calcula middle tal que s
      &lt; middle &lt; e. Pela hipótese indutiva, as linhas 5 e 6 executam
      `merge-sort` com sucesso, pois |A[s:middle]| &lt; n e
      |A[middle:e]| &lt; n. Conforme visto anteriormente, `merge` executa
      corretamente na linha 7. Logo, A[s:e] será uma permutação ordenada de
      A'[s:e] onde A' é o array A anteriormente à execução do algoritmo. Q.E.D.
