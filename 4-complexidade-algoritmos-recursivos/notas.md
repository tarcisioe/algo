Aula 4 - Complexidade de Tempo em Algoritmos Recursivos
=======================================================

Recorrências
------------

* Para expressar o tempo de execução de algoritmos recursivos serão utilizadas
  recorrências.

* Recorrências são expressões recursivas para uma função que descreve o tempo
  necessário para o algoritmo rodar sobre uma entrada de tamanho n. Por exemplo:
  ```
  1. T(1) = Θ(1)
  2. T(n) = 2T(n/2) + Θ(n)
  ```

    * A recorrência descreve então:
        1. O tempo necessário para a execução do caso de tamanho 1. Em geral,
           a complexidade será Θ(1), pois o tamanho do subproblema base é
           constante.
        2. O tempo necessário para rodar casos onde a recursão acontece.

    * No exemplo acima, o tempo para executar sobre uma entrada de tamanho n
      envolve duas recursões em subproblemas de metade do tamanho, mais uma
      operação linear.

    * É possível ler a expressão em mais detalhes:

      ```
      T(n) = 2 T(n/2) + Θ(n)
            /     \         \
         número    tamanho   operações não-recursivas a cada passo
           de        do
        recursões   subproblema
      ```

    * Conforme é possível notar, a recursão acima descreve o tempo de execução
      do algoritmo `merge-sort`. Ocorrem duas recursões sobre metade da entrada,
      ou seja, a ordenação de cada uma das metades, e uma operação linear além
      da recursão, no caso a execução da rotina `merge`.


Extraindo a Recorrência de um Algoritmo
---------------------------------------

* No caso geral, a recorrência que descreve o tempo de execução de um algoritmo
  é extraída de forma relativamente direta da estrutura deste. Tome-se como
  exemplo o algoritmo `binary-search-r`.

  ```pseudo
 1. binary-search-r(array A, index s, size n, key k):
 2.     if n == 0:
 3.         return False
 4.
 5.     half = n/2
 6.     middle = s + half
 7.
 8.     if A[middle] == k:
 9.         return True
10.     else if k < A[middle]:
11.         return binary-search-r(A, s, half, k)
12.     else:
13.         return binary-search-r(A, middle + 1, n - half - 1, k)
```

* Em uma análise semelhante à forma das análises da aula 3, todas as linhas,
  exceto 11 e 13, possuem complexidade constante. Isso é claro pois não existem
  laços nem chamadas a outras subrotinas.

* Nas linhas 11 e 13, `binary-search-r` é novamente invocado, porém com
  parâmetros diferentes. Conforme visto durante a análise de corretude, cada uma
  dessas invocações ocorre sobre uma entrada de tamanho aproximadamente n/2.

* Um detalhe importante no caso é o seguinte: as linhas 11 e 13 são mutuamente
  exclusivas. Portanto, apesar de haverem duas chamadas recursivas no corpo
  do algoritmo, apenas um será chamado.

* Desta forma, sabe-se que o algoritmo é composto por:
    1. Trabalho constante (Θ(1)), envolvendo decidir em qual metade do array
       recursionar.
    2. Apenas um passo recursivo em metade do tamanho da entrada.

* Por fim, sabe-se que no caso de n ter tamanho 1, ou a linha 9 executa, ou
  a recursão executará apenas as primeiras 2 linhas, ou seja, o tempo será
  constante.

* Logo, temos a seguinte recorrência:

  ```
  T(1) = Θ(1)
  T(n) = T(n/2) + Θ(1)
  ```


Analisando Recorrências
-----------------------

* Recorrências, por sua própria natureza recursiva, são difíceis de se
  interpretar da mesma forma que expressões fechadas, como o tempo de execução
  de algoritmos puramente iterativos.

* É necessário, então, extrair uma forma fechada da recursão, para poder
  comparar o tempo de execução de algoritmos recursivos no mesmo patamar que foi
  visto anteriormente, ou seja, via análise assintótica com uma função simples
  sobre o tamanho da entrada.

* Existem diversos métodos para análise de recorrência. Aqui serão demonstrados
  três:
    1. Técnica da árvore de recursão.
    2. O _Master Method_.
    3. Técnica da substituição (_guess-and-prove_).

* Serão vistos aqui os dois primeiros métodos. O método da substituição nada
  mais é do que a prova de uma hipótese previamente tomada via indução.
  É possível encontrar uma boa descrição do método da substituição no
  capítulo 4.3 do livro _Introduction to Algorithms, 3rd. ed._.


Árvore de Recursão
------------------

* A técnica da árvore de recursão é possivelmente a forma mais direta de se
  imaginar como o tempo de execução de um algoritmo recursivo se compõe.

* A ideia é efetivamente representar a execução do algoritmo como uma árvore,
  onde cada chamada é um nó, e os nós filhos de cada chamada são as chamadas
  geradas durante a sua execução.

* Cada nó possui um custo. Esse custo é o tempo de execução das operações
  não-relacionadas à recursão (por exemplo, a invocação de `merge` em
  `merge-sort`).

* Para o exemplo do algoritmo `merge-sort`, a árvore irá ficar da seguinte
  forma:

  ```
                             Θ(n)
                    /                    \
                  Θ(n/2)               Θ(n/2)
              /            \        /          \
           Θ(n/4)       Θ(n/4)   Θ(n/4)     Θ(n/4)
            ...           ...     ...         ...
    Θ(1) Θ(1) Θ(1) Θ(1) Θ(1)  ... Θ(1) Θ(1) Θ(1) Θ(1) Θ(1)
  ```

* O tempo total é nada mais que a soma do tempo de execução de cada nó da
  árvore.

    * Para a análise de `merge-sort`, supõe-se que o tamanho n da entrada seja
      um número n = 2^k, ou seja, uma potência de 2. Isto é apenas para evitar
      o uso de teto e chão na análise, e tornar a árvore de recursão sempre uma
      árvore binária completa. Para números que não sejam potências de 2, a
      altura da árvore ainda pode ser expressada da mesma forma, porém com
      aproximações.

    * Para a árvore de `merge-sort`, é possível notar dois detalhes importantes
      (j inicia em 0):
        1. Em cada nível j da árvore, o denominador em cada nó é 2^j.
        2. Em cada nível j da árvore, existem 2^j nós.

    * Porém, no último nível da árvore existem ao máximo n nós. Para observar
      isto, basta lembrar que os nós folha da árvore de recursão são os
      casos base. Ou seja, em `merge-sort`, quando o subarray possui tamanho
      1. O número de subarrays de tamanho 1 em um array de tamanho n é n.

    * Sabendo disso, percebe-se que o valor máximo de j (denominado k) é
      log(n). Isto ocorre pois 2^k = n, segundo o fato 2 acima, logo k = log(n).

    * Desta forma, existem log(n)+1 níveis na árvore. Por fim, percebe-se que a
      soma em cada nível é sempre de 2^j nós de complexidade n/2^j. Desta
      forma, a complexidade total é 2^(j)*n/2^(j), ou seja, n.

    * Conclui-se então que a soma de todos os nós é de complexidade Θ(n*log(n)).

* O método da árvore de recursão é altamente genérico, porém demanda uma análise
  única para cada árvore de recursão encontrada. Em geral, ele é utilizado como
  uma ferramenta para gerar uma boa hipótese a ser testada via o método da
  substituição. Ele também é utilizado na prova do método que será visto a
  seguir, o _Master Method_.

Master Method
-------------

* O _Master Method_ é uma forma de extrair diretamente a complexidade a partir
  de recursões em uma forma específica.

* O _Master Method_ pode ser usado como um conjunto de regras simples, porém
  entender seu funcionamento mais a fundo também é interessante.

* A definição do _Master Method_ depende do chamado "_Master Theorem_",
  enunciado a seguir:

    * Dada uma recorrência na forma:
        1. T(1) = Θ(1)
        2. T(n) = a(n/b) + Θ(n^(d))

    * Onde:
        * a -> número de chamadas recursivas (> 0)
        * b -> fator de encolhimento do tamanho da entrada (> 1)
        * d -> expoente do tempo de execução das operações fora da recursão

    * Então:
        * Se a &lt; b^d, T(n) = Θ(n^(d))
        * Se a = b^d, T(n) = Θ(n^(d)*log(n))
        * Se a > b^d, T(n) = Θ(n^(logb(a)))

* Primeiro exemplo:
    * T(n) = 2(n/2) + Θ(n)

    * Neste caso:
        1. a = 2
        2. b = 2
        3. d = 1

    * b^d = 2^1 = 2, logo a = b^d.

    * Portanto, T(n) = O(n^(d)\*log(n)), ou seja, O(n\*log(n))

    * A recorrência é a que descreve o tempo de execução de `merge-sort`,
      logo é verificável que o _Master Method_ produz um resultado correto
      neste caso.

Provando o Master Method
------------------------

* Nesta prova, será assumido o seguinte:
    * A recorrência é da forma:
        1. T(n) = Θ(1)
        2. T(n) = aT(n/b) + c*n^(d)

    * n é uma potência de b (o caso geral possui uma prova semelhante,
      porém ligeiramente mais complexa).

    * **IMPORTANTE**: Não se pode, em um caso geral, produzir provas envolvendo
                      notação assintótica apenas em cima de um domínio limitado.
                      Por exemplo, para a seguinte função uma análise somente
                    sobre as potências de 2 seria falha:
        * T(n) =
            * n se n = 2^(k) para algum k inteiro positivo.
            * n² em qualquer outro caso.

* Primeiramente, seja uma árvore de recursão, como a utilizada anteriormente
  na análise de `merge-sort`, para a recorrência. Para cada nível j da árvore,
  existem a^j nós, cada um sobre uma entrada de tamanho n/(b^(j)). Isto porque
  a árvore possui apenas uma raiz, e ocorrem a chamadas a partir de cada
  chamada recursiva. Da mesma forma, a cada recursão, o tamanho n da entrada é
  dividido por b.

* Também pelo mesmo argumento visto anteriormente, é possível definir a altura
  da árvore de recursão como logb(n).

* Dado um nível j da árvore de recursão, existirão a^(j) nós de tamanho n/b^(j).
  Define-se então o trabalho executado em um nó no nível j como:
    * tj(n) = c*(n/b^(j))^(d)

* Portanto, o trabalho total no nível j é:
    * Tj(n) = a^(j) * c*(n/b^(j))^(d)

* Com alguma manipulação algébrica, obtém-se:
    * Tj(n) = c*n^(d) * (a/b^(d))^(j)

* Somando sobre todos os níveis:
    ```
                  logb(n)
    T(n) = cnᵈ *    Σ (a/bᵈ)ʲ
                   j=0
    ```

* Dando uma interpretação a cada parte do resultado, temos:
    1. a -> O fator de proliferação dos subproblemas.
    2. b^(d) -> O fator de diminuição da quantidade de trabalho.
        * É importante perceber que o trabalho não é dividido por b apenas.
          No caso, por exemplo, de que d = 2, a complexidade de tempo fora
          da recursão é da ordem de n², ou seja, se b = 2, (n/2)² = n²/4,
          ou seja, o trabalho foi divido por 4, e não apenas por 2.

* Isto já deixa mais clara a interpretação dos casos do _Master Theorem_:
    1. Se a &lt; b^d, os subproblemas se proliferam menos do que o trabalho
       diminui.
    2. Se a = b^d, os subproblemas se proliferam no mesmo ritmo em que o
       trabalho diminui.
    3. Se a > b^d, os subproblemas se proliferam mais rápido do que o
       trabalho diminui.

* Intuitivamente, nos 3 casos, o que ocorre é o seguinte, respectivamente:
    1. O trabalho executado na raiz domina o trabalho nos níveis abaixo.
    2. O trabalho em cada nível é igual.
    3. O trabalho nas folhas domina o trabalho nos níveis acima.

* Assim, pode-se esperar que ocorra:
    1. Trabalho = Θ(n^(d)) (o trabalho na raiz)
    2. Trabalho = Θ(n^(d)*log(n)) (o trabalho em todos os níveis)
    3. Trabalho = Θ(#folhas) (o número de folhas na árvore de recursão, pois
       cada folha possui trabalho Θ(1)).

* Analisando a expressão de T(n) em cada caso, tem-se:
    1. Para a &lt; b^d:
       ```
                     logb(n)
       T(n) = cnᵈ *    Σ (a/bᵈ)ʲ
                      j=0

            = cnᵈ * Θ(1)
       ```
        * Isso ocorre pois se a &lt; b^d, a/b^(d) &lt; 1. A soma infinita de
          (a/b^(d))^j sobre j é uma série geométrica de razão
          r = a/b^(d) &lt; 1, com resultado 1/(r-1) (pois a série converge).
          Logo, a soma até logb(n) precisa ser menor que 1/(r-1), que é
          uma constante, logo Θ(1).
    2. Para a = b^d:
       ```
                     logb(n)
       T(n) = cnᵈ *    Σ (a/bᵈ)ʲ
                      j=0

            = cnᵈ * Θ(log(n))
       ```
        * Isso ocorre pois se a = b^d, a/b^(d) = 1, logo a soma de 0 até logb(n)
          é logb(n) + 1, ou seja, Θ(log(n)).
    3. Para a > b^d:
       ```
                     logb(n)
       T(n) = cnᵈ *    Σ (a/bᵈ)ʲ
                      j=0

            = cnᵈ * Θ((a/bᵈ)^logb(n))

            = cnᵈ * Θ(a^logb(n) * 1/nᵈ)
       ```
        * Neste caso, isso ocorre pois o termo (a/b^(d))^logb(n) é o termo
          de maior ordem na soma. Na sequência, ao elevar b a logb(n) obtém-se
          n.

* Estendendo a notação assintótica para a expressão toda nos 3 casos:
    1. Trivialmente, Θ(n^(d))
    2. Θ(n^(d)*log(n))
    3. Neste caso, ainda é necessário notar alguns detalhes:
        * A expressão se tornará Θ(n^(d) * a^logb(n) * 1/n^(d)). Os termos
          n^d e 1/n^(d) se cancelam, sobrando Θ(a^logb(n) ).
        * a^logb(n) é precisamente o número de folhas na árvore de recursão,
          pois logb(n) é o último nível j da árvore. Como cada nível j possui
          a^j nós, o número de folhas é a^logb(n). Isso confirma a intuição
          anterior.
        * Por fim, a^logb(n) = n^logb(a), o que é verificável tomando
          o logaritmo em base b dos dois lados:
          * logb(a^logb(n) ) = logb(n)*logb(a)
          * logb(n^logb(a) ) = logb(a)*logb(n)
        * Portanto, tem-se nesse último caso Θ(n^logb(a) ). Q.E.D.


Exemplos do Master Method
-------------------------

* Agora que foi explicitada a prova do _Master Theorem_, é interessante ver
  alguns exemplos da aplicação do método:

* Nota: em todos os casos, T(1) = Θ(1).

* T(n) = (n/2) + Θ(1) (`binary-search-r`):
    1. a = 1
    2. b = 2
    3. d = 0

    * a = b^d, logo o segundo caso será usado. Tem-se então:
        * Θ(n^d * log(n)) = Θ(n^0 * log(n)) = Θ(1*log(n)) = Θ(log(n)).

* T(n) = 4(n/2) + Θ(n) (`recursive-multiply`):
    1. a = 4
    2. b = 2
    3. d = 1

    * a > b^d, logo o terceiro caso será usado. Tem-se então:
        * Θ(n^logb(a) ) = Θ(n^log2(4) ) = Θ(n²).

* Nota-se aqui, então, que a complexidade de `recursive-multiply` é
  a mesma que a do algoritmo de multiplicação iterativo.

* T(n) = 3(n/2) + Θ(n) (`karatsuba-multiply`):
    1. a = 3
    2. b = 2
    3. d = 1

    * a > b^d, então o terceiro caso será usado novamente. Tem-se então:
        * Θ(n^logb(a) ) = Θ(n^log2(3) ) = Θ(n^(1.58...)).

* T(n) = 2(n/2) + Θ(n²) (exemplo fictício):
    1. a = 2
    2. b = 2
    3. d = 2

    * a &lt; b^d, logo o primeiro caso será usado. Tem-se então:
        * Θ(n^(d)) = Θ(n²).


Notas Finais
------------

* O _Master Method_, como é notável, só permite calcular a complexidade a
  partir de recorrências onde:
    * Só existam subproblemas de tamanho igual.
    * O trabalho por nó seja polinomial em n.

* Existem generalizações do _Master Method_ para abrangência maior, por
  exemplo, permitindo um fator logarítmico no trabalho por nó, ou
  permitindo subproblemas de tamanho diferente.

* Existem também definições razoavelmente diferentes do _Master Method_.
  Uma pode ser encontrada no livro _Introduction to Algorithms, 3rd Ed._,
  nos capítulos 4.5 e 4.6.

