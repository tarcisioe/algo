Aula 3 - Complexidade de Tempo e Análise Assintótica
====================================================

Tempo de Execução
-----------------

* O que é o tempo de execução de um algoritmo?
    * De forma simplística: o tempo que um algoritmo leva para produzir uma
      saída.

* Isto pode depender de diversos fatores:
    * O algoritmo em si.
    * Qualquer subrotina utilizada pelo algoritmo.
    * O tamanho da entrada (em geral, o tamanho da entrada é o ponto chave
      da análise).

* Para a análise de tempo de execução, as pistas a serem seguidas são:
    * Operações primitivas:
        * Estas são as operações básicas disponíveis, como por exemplo:
            * Na multiplicação de inteiros de n dígitos: somar ou multiplicar
              dois inteiros de tamanho 1.
            * Para busca e ordenação por comparação:
              comparar dois elementos, trocar elementos de lugar.
            * Em um caso geral: atribuições, incrementar contadores, etc.
        * Em uma descrição mais realista, qualquer operação que seja composta
          por um número fixo de instruções de máquina, que não gerem um laço nem
          invoquem algum outro subprocedimento.

    * Laços:
        * Em geral, laços terão uma condição de parada (a condição de um laço
          `while`, o valor máximo de um contador em um `for`, por exemplo).
        * O tempo de execução máximo de um laço será a soma de todas as
          operações executadas a cada iteração, multiplicado pelo número de
          iterações.
        * Para laços `for` de contagem simples, isto é normalmente fácil de
          analisar.
        * Para laços `while` ou laços com um `break` em seu corpo, uma análise
          mais detalhada pode estar envolvida.

    * Condicionais:
        * O tempo de execução de um condicional depende do resultado de sua
          condição, mas será no mínimo uma constante (a checagem da condição).
        * Se ambos os casos no condicional possuem o mesmo tempo de execução,
          este mais o tempo de checar a condição serão o tempo de execução do
          condicional.
        * Se não, para análise de pior caso (a forma mais comum de análise),
          geralmente o maior dos tempos será utilizado.
        * Isto pode demandar uma análise mais detalhada se o algoritmo
          claramente limita o número de vezes que um condicional toma o pior
          caso, por exemplo.

    * Processos recursivos:
        * Estes em geral são mais difíceis de analisar. Existem diversos métodos
          para análise de processos recursivos, mas em geral todos iniciarão com
          uma recorrência descrevendo o tempo de execução de forma recursiva.
          Isto será visto com mais detalhe adiante.


Compondo o Tempo de Execução de um Algoritmo
--------------------------------------------

* O tempo de execução de um algoritmo iterativo será simplesmente a soma de seus
  componentes:

    ```pseudo
     1. multiply(integer x, integer y, size n, base b):
     2.     reverse x and y
     3.     results = int[n][2*n]
     4.     initialize results with 0s
     5.
     6.     for i = 0 to n-1:
     7.         carry = 0
     8.         for j = 0 to n - 1:
     9.             c1, m = times(x[j], y[i], b)
    10.             c2, m = plus(m, carry, b)
    11.             _, carry = plus(c2, c1, b)
    12.             results[i][j + i] = m
    13.         results[i][n+i] = carry
    14.         reverse results[i]
    15.
    16.     result = int[2*n]
    17.     sum results columnwise with carries and place into result
```

    * Ao longo da análise, constantes cn serão mencionadas. Elas representam
      custos constantes, possivelmente diferentes.

    * Em ordem, as linhas possuem custo:
        ```
         2. 2n*c1, pois x e y possuem tamanho n.
         3. 2n²*c2, pois 2*n² elementos são alocados.
         4. 2n²*c3, pois cada elemento em result é inicializado.
         6. Aqui inicia um somatório dos custos das linhas 7 a 14.
            7. c3, é apenas uma inicialização
            8. Aqui inicia outro somatório das linhas 9 a 12.
                9. c4, pois multiplicação de números de um dígito é constante.
               10. c5, pois soma de números de um dígito é constante.
               11. c5 novamente.
               12. c6, pois atribuições são constantes.
           13. c6, o custo de uma atribuição.
           14. 2n*c1, pois o tamanho de uma linha de results é 2n.
        16. c6.
        17. 2n²*c7, pois existem n resultados de tamanho 2n.
        ```

    * Logo, temos o seguinte custo:
        ```
        (2*c1)*n + (2*c2)n² + (2*c3)n² + Cost(L7..14) + c6 + (2*c7)n²

        =

        2(c2 + c3 + c7)n² + (2*c1)n + Cost(L7..14) + c7
        ```

        * Onde `Cost(L7..14)` é:
            ```
            n-1     n-1
             Σ (c3 + Σ (c4 + 2*c5 + c6) + c6 + (2*c1)n)
            i=0     j=0

             =

            n-1                      n-1
             Σ ((c3 + c6 + (2*c1)n) + Σ (c4 + 2*c5 + c6))
            i=0                      j=0

             =
                                   n-1 n-1
             (c3 + c6 + (2*c1)n)n + Σ   Σ  (c4 + 2*c5 + c6)
                                   i=0 j=0

             =

             (2*c1 + c4 + 2*c5 + c6)n² + (c3+c6)n
            ```

    * Portanto:
        ```
        Seja C(n) o custo de multiply para uma entrada de tamanho n:

        C(n) = 2(c2 + c3 + c7)n² + (2*c1)*n + (2*c1 + c4 + 2*c5 + c6)n² + (c3+c6)n + c6
             = (2*c1 + 2*c2 + 3*c3 + c4 + 2*c5 + c6 + 2*c7)n² + (2*c1+c3+c6)n + c6
        ```

    * O tempo de execução preciso, considerando todas as constantes listadas, é o
      escrito acima. Porém, é possível já perceber a possibilidade da existência
      de constantes não mencionadas na análise (cálculo dos índices, incremento do
      contador do laço, etc). Esta é uma das razões pela qual a análise com todas
      as constantes, ao menos inicialmente, é mais confusa do que o necessário
      para um entendimento do tempo de execução do algoritmo.

    * Para simplificar a expressão, seja c = max(c1..c8).
        ```
        C(n) ≤ (12c)n² + (4c)n + c
        ```

Laços Com Término Menos Simples
-------------------------------

* Para outros algoritmos iterativos, a dedução do número de vezes que o laço
  executa não é tão simples:

    ```pseudo
     1. binary-search-i(array A, index s, size n, key k):
     2.     while n > 0:
     3.         half = n/2
     4.         middle = s + half
     5.         if A[middle] == k:
     6.             return True
     7.         else if k < A[middle]:
     8.             n = half
     9.         else:
    10.             s = middle + 1
    11.             n = n - half - 1
    12.
    13.     return False
```

    * Nesse caso, é necessário analisar a forma como n decresce, para analisar
      o número de iterações máximo necessário para que n ≤ 0.

    * Nas linhas 8 e 11 do algoritmo, n é reatribuido para n/2 [8] ou
      n - (n/2) - 1 [11], lembrando que / é divisão inteira, pois n é inteiro.

    * Considerando que há um número inteiro positivo n sendo dividido por 2,
      seja n = 2^k. Como a divisão inteira é o chão da divisão nos números
      reais:

        ```
        n = 2^k
        k = log(n)

        n * 1/(2^k) = 1
        ⌊n * 1/(2^k) * 1/2⌋ = 0
        ⌊n * 1/2^(k+1)⌋ = 0
        ```

    * Logo, n precisa ser dividido k+1 = log(n) + 1 vezes para se tornar 0.
      Com base nisso, pode-se analisar o algoritmo:

        * Nota: será convencionado c como o custo de todas as operações
          constantes, sendo o custo da operação constante de maior tempo.

        ```
         2. Aqui inicia um somatório dos custos das linhas 3 a 11.
             3. c
             4. c
             5. É necessário avaliar os três caminhos do condicional, mas aqui
                há uma constante c da comparação.
                 6. c
             7. Somatório de c + C(8)
                 8. c
             9. C(10) + C(11) (o else não tem custo pois a comparação já foi
                contada)
                10. c
                11. c
        13. c
        ```

    * O custo total então (com c maximizado) será:
        ```
        log(n)+1
            Σ (c + c + c + c + max(c, c, c + c)) + c
           i=1

        =

            (6c)(log(n)+1) + c

        =

            (6c)log(n) + 7c
        ```

    * Podemos dizer então que
        ```
        C(n) ≤ (6c)log(n) + 7c
        ```

    * É notável que a análise demonstra o tempo no pior caso (k não está no
      array A). Em geral, quando o algoritmo se comporta diferentemente
      dependendo de circunstâncias, o tempo que se deseja saber é o pior caso.
      Para alguns algoritmos, esse pior caso pode ser altamente controlado, e o
      caso interessante passará a ser o caso médio (o algoritmo Quick Sort é um
      exemplo, e será visto adiante).


Análise Assintótica
-------------------

* Em geral, o tempo de execução de algoritmos não é discutido ao nível de total
  realismo, ou seja, considerando os custos reais de cada operação. Isso ocorre
  por dois principais motivos:
    * O custo de cada operação primitiva pode ser diferente dependendo da
      máquina, da linguagem e do compilador, por exemplo.
    * Dadas entradas de tamanho suficiente, se duas funções que descrevem tempos
      de execução possuírem ordem de crescimento diferente, a função de menor
      ordem de crescimento resultará em um resultado menor.

* Uma coisa a sempre ser considerada: No sentido mais geral, análise de
  algoritmos tende a considerar cenários de pior caso com entradas do maior
  tamanho possível.

* Análise assintótica é útil exatamente pelos motivos acima:
    * Suprime constantes, ou seja, remove informação que não é realmente
      independente de plataforma, compilador, linguagem, etc.
    * Preserva a ordem de crescimento (de fato, análise assintótica é a
      ferramenta principal para discutir ordem de crescimento).

* Nota: quando necessário, otimizar constantes e levar em consideração detalhes
  de plataforma não é de forma alguma inútil. Análise assintótica é, portanto,
  uma ferramenta para analisar algoritmos de um ponto de vista teórico, antes
  de considerar implementações reais, porém muito importante para tomar diversas
  decisões.

A notação O
-----------

* A notação O é uma forma de categorizar funções baseado em sua ordem de
  crescimento. É basicamente uma ferramenta para comparar a forma como duas
  funções crescem.

* Atenção: todas as funções aqui discutivas são crescentes e positivas, ou seja,
  para n1 &lt; n2, 0 &lt; f(n1) ≤ f(n2).

* Definição: para alguma função f, O(f) é o conjunto de todas as funções g que
  se encaixam na seguinte definição:
    ```
    g(n) ∈ O(f(n)) <=> ∃ c, n0 > 0 | g(n) ≤ c*f(n) ∀ n ≥ n0
    ```

    * Ou seja, é possível nomear um valor n0 para n a partir do qual g(n) é
      menor ou igual a f(n) multiplicada por um valor constante qualquer.

* Exemplos:
    1. f(n) = 2n ∈ O(n):
        ```
        Seja n0 = 1 e c = 2. ∀ n ≥ n0, 2n ≤ 2n, Q.E.D.
        ```
    2. f(n) = (12k)n² + (4k)n + k ∈ O(n²) (a mesma função da complexidade do
      algoritmo da multiplicação).
        ```
        Seja n0 = 1 e c = 17k.
        f(n) = (12k)n² + (4k)n + k
             ≤ (12k)n² + (4k)n² + (k)n²
             ≤ (17k)n²
             ≤ c*n²

        Logo, f(n) ≤ c*n² ∀ n ≥ n0, Q.E.D.
        ```
    3. f(n) = ak\*n^k + ... + a1\*n^1 + a0 ∈ O(n^k):
        ```
        Seja n0 = 1 e c = |ak| + ... |a1| + |a0|.
        Neste caso, ∀ n ≥ n0 = 1, f(n) ≤ |ak|n^k + ... + |a1|n + |a0|.
                                       ≤ |ak|n^k + ... + |a1|n^k + |a0|n^k
                                       ≤ c*n^k.

        Q.E.D.
        ```
    4. Para todo k ≥ 1, n^k ∉ O(n^(k-1))
        ```
        Por contradição: Esteja n^k em O(n^(k-1)).
                         Então existe c, n0 > 0 | n^k ≤ c*n^(k-1) ∀ n ≥ n0.
                         Mas então, cancelando n^(k-1) de ambos os lados,
                         n ≤ c ∀ n ≥ n0. Isto não pode ocorrer, pois c é uma
                         constante. Q.E.D.
        ```

* Interessante notar que nos exemplos 2 e 3 c e n0 foram na realidade obtidos
  por análise reversa do último passo, ou seja, os passos foram tomados,
  chegando no valor de c necessário.

* A notação O age como um "menor ou igual" para ordens de crescimento, ou seja,
  dado, g(n) ∈ O(f(n)), g(n) cresce no máximo tão rápido quanto f(n). As outras
  notações compõem o resto das comparações.

A notação Ω
-----------

* Definição: para alguma função f, Ω(f) é o conjunto de todas as funções g que
  se encaixam na seguinte definição:
    ```
    g(n) ∈ Ω(f(n)) <=> ∃ c, n0 > 0 | g(n) ≥ c*f(n) ∀ n ≥ n0
    ```

* Exemplos:
    1. f(n) = (12k)n² + (4k)n + k ∈ Ω(n²):
        ```
        Seja n0 = 1 e c = 1.
        f(n) = (12k)n² + (4k)n + k
             ≥ (12k)n² + (4k)n
             ≥ (12k)n²
             ≥ n²

        Logo, f(n) ≥ c*n² ∀ n ≥ n0, Q.E.D.
        ```

* Ω portanto possui a mesma definição de O, porém para valores maiores ou
  iguais. Ou seja, Ω define uma comparação de "maior ou igual" entre funções.
  Assim, se g(n) ∈ Ω(n), g(n) cresce no mínimo tanto quanto f(n).

* Ω então será utilizado normalmente para tratar de limites inferiores. Será
  a ferramenta utilizada na discussão de limites inferiores para ordenação e
  busca por comparação, por exemplo.

A notação Θ
-----------

* Θ faz o papel de "igualdade" para ordens de crescimento. O paralelo entre
  notações assintóticas e comparadores se mantém: Se alguma função
  g(n) ∈ O(f(n)) e g(n) ∈ Ω(f(n)), g(n) ∈ Θ(f(n)), e vice versa.

* Definição: para alguma função f, Θ(f) é o conjunto de todas as funções g que
  se encaixam na seguinte definição:
    ```
    g(n) ∈ Θ(f(n)) <=> ∃ c1, c2, n0 > 0 | c1*f(n) ≤ g(n) ≤ c2*f(n) ∀ n ≥ n0
    ```

* Definição alternativa:
    ```
    g(n) ∈ Θ(f(n)) <=> g(n) ∈ O(f(n)) ∧ g(n) ∈ Ω(f(n)).
    ```

* Exemplos:
    1. f(n) = (12k)n² + (4k)n + k ∈ Θ(n²):
        ```
        Conforme visto anteriormente, f(n) ∈ O(n²) ∧ f(n) ∈ Ω(n²).
        Logo f(n) ∈ Θ(n²), Q.E.D.
        ```
    2. f(n) = (6k)log(n) + 7k ∈ Θ(log(n)):
        ```
        Seja c1 = 1, c2 = 13k, n0 = 1.
        f(n) = (6k)log(n) + 7k
             ≥ (6k)log(n)
             ≥ log(n)
             ≥ c1*log(n)

        f(n) = (6k)log(n) + 7k
             ≤ (6k)log(n) + (7k)log(n)
             ≤ (13k)log(n)
             ≤ c2*log(n)

        Logo, c1*log(n) ≤ f(n) ≤ c2*log(n) ∀ n ≥ n0, Q.E.D.
        ```
    3. Sejam f(n) e g(n) funções positivas. max(f(n), g(n)) ∈ Θ(f(n)+g(n)):
        ```
        Seja c1 = 1/2, c2 = 1, n0 = 1.
        f(n) = max(f(n), g(n))
             ≥ (f(n) + g(n))/2 (pois o máximo entre dois valores precisa
                                ser maior que a média entre eles)
             ≥ 1/2 * (f(n) + g(n))
             ≥ c1*(f(n)+g(n))

        f(n) = max(f(n), g(n))
             ≤ f(n) + g(n)
             ≤ c2*(f(n)+g(n))

        Logo, c1*(f(n)+g(n)) ≤ f(n) ≤ c2*(f(n)+g(n)) ∀ n ≥ n0, Q.E.D.
        ```

* Em geral, a descrição de tempo de execução de algoritmos deverá utilizar Θ,
  pois a descrição da ordem de crescimento costuma ser o mais exata possível.

As notações o e ω
-----------------

* o e ω completam as notações fazendo o papel de menor e maior, respectivamente.
  A analogia continua se mantendo, então O -> ¬ω, ω -> ¬O, Ω -> ¬o,
  o -> ¬Ω, o v ω -> ¬Θ.

* Definição: para alguma função f, o(f) é o conjunto de todas as funções g que
  se encaixam na seguinte definição:
    ```
    g(n) ∈ o(f(n)) <=>  ∀ c > 0, ∃ n0 > 0 | g(n) ≤ c*f(n) ∀ n ≥ n0
    ```

* Exemplo:
    1. f(n) = 2^n ∈ o(2^(10n))
        ```
        Seja c > 0. Seja n0 = 1.

        f(n) = 2^n
             ≤ c*2^n
             ≤ c*2^(10n)

        Logo, ∀ c > 0, f(n) ≤ c*2^(10n) ∀ n ≥ n0. Q.E.D.
        ```
    2. f(n) = 2^(n) ∉ o(2^(n+10)):
        ```
        Seja c = 1/2048 e n0 > 0.

        f(n) = 2^(n)


        Logo, ∃ c > 0 | f(n) > c*f(n) ∀ n ≥ n0, logo a definição falha (a
        existência da negação nega o "para todo c"). Q.E.D.
        ```

* É interessante perceber que o segundo exemplo também prova que
  2^(n+10) ∈ Ω(2^(n)).

* A definição de ω é simétrica, trocando apenas ≤ por ≥.
