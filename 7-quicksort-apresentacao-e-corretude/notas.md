Aula 7 - Quick Sort: Apresentação e Corretude
=============================================

* Nesta aula será apresentado mais um algoritmo de ordenação, o _Quick Sort_.
* Uma pergunta possível é: para que mais um método de ordenação? Motivos são:
    * Análise de detalhes de cada método.
    * No caso do Quick Sort, uma análise de complexidade com mais nuances.
    * Uso mínimo de memória auxiliar.
    * Constantes em geral melhores.


Ideia Inicial
-------------

* Como o _Merge Sort_, o Quick Sort é um algoritmo de divisão-e-conquista, ou
  seja, seu princípio é resolver subproblemas menores e com base nestas soluções
  construir a resposta do problema maior.

* Relembrando a estrutura básica do Merge Sort (ignorando o caso base):
    1. Encontrar ponto médio do array (Θ(1)).
    2. Ordenar cada uma das metades (2\*T(N/2)).
    3. Combinar as metades em um array ordenado (Θ(n)).

* Com isso, obtém-se um algoritmo correto e de complexidade deterministicamente
  Θ(n\*log(n)), para qualquer entrada.

* Conforme será visto, a corretude do Quick Sort é verificável de forma
  semelhante à do Merge Sort. Já a complexidade faz necessária uma análise mais
  detalhada.

* No Quick Sort, a ideia central em vez de um passo de _merge_ será um passo de
  particionamento em torno de um pivô. Também é interessante notar que este
  passo ocorre antes da recursão, em contraste com o _merge_, que ocorre depois.

* A estrutura básica do Quick Sort então é (ignorando o caso base, que é o
  mesmo, ou seja, array de tamanho &lt; 2).
    1. Particionar o array em torno de um pivô conforme descrito adiante.
    2. Ordenar a parte anterior ao pivô e a parte posterior ao pivô.

* A chave para a corretude do algoritmo é a forma como se faz o particionamento,
  conforme será visto a seguir.


Particionando em torno de um pivô
---------------------------------

* O primeiro passo é nomear um elemento `A[p]` do array `A` a ser ordenado como
  "pivô". A princípio não será levada em conta a forma como o pivô é escolhido,
  apenas se assumirá que isto é feito em tempo Ω(n).

* Para manter a explicação mais concreta, a estratégia utilizada será sempre
  escolher o primeiro elemento no intervalo a ser ordenado.

* O passo de particionamento então será um algoritmo capaz de resolver o
  problema do particionamento, ou seja:
    * Input: um subarray `A[s:e]` e um índice i, s ≤ i ≤ e.
    * Output: dois subarrays B' e B'', tal que B' contenha todos os elementos em
              A[s:e] menores ou iguais a A[i] (menos o próprio A[i]) e B''
              contenha os maiores que A[i].

* Em um exemplo pictorial:
```
                   i
A = [             |x|                  ]

                       j
B = [   B': ≤ x       |x|  B'': > x    ]
```

* O detalhe importante deste processo de particionamento é o seguinte:
    * Existe uma permutação ordenada de A em que A[i] se encontra exatamente
      na posição j em que se encontra em B.

* Em outras palavras:
    * Após o processo de particionamento, A[i] é reposicionado para sua
      posição final no array ordenado.


Quick Sort não-_in-place_
-------------------------

* Inicialmente será descrito uma versão não _in-place_ do algoritmo, que
  será compatível com a primeira estratégia de particionamento apresentada.
    * **NOTA**: Esta versão do algoritmo é extremamente ineficiente em uso de
                memória e serve apenas para ilustrar o funcionamento básico.

```pseudo
 1. quicksort-nip(A, s, e):
 2.     if e - s < 2:
 3.         return A[s:e]
 4.
 5.     pivot = choose-pivot(A, s, e)
 6.
 7.     left, right = partition-nip(A, s, e, pivot)
 8.
 9.     return (quicksort-nip(left, 0, len(left)) +
10.             A[pivot] +
11.             quicksort-nip(right, 0, len(right)))
```

* Presumindo `partition-nip` correto, é possível provar `quicksort-nip` correto
  por indução:
    * Passo base: para um array A de tamanho n &lt; 2, A está trivialmente
                  ordenado, e é retornado na linha 3.
    * Hipótese indutiva: dado um inteiro positivo n, `quicksort-nip` está
                         correto para qualquer entrada de tamanho m &lt; n.
    * Passo indutivo: Seja n ≥ 2. Seja um subarray A[s:e] de tamanho n.
                      Conforme a definição de partição, `pivot` está em
                      uma posição válida para uma permutação ordenada de A.
                      Pela hipótese indutiva, nas linhas 9 e 11 `quicksort-nip`
                      está correto, pois as entradas tem tamanho m &lt; n.
                      Logo, o resultado é a concatenação entre um subarray
                      ordenado composto por elementos menores ou iguais a
                      `pivot`, o próprio `pivot` e um subarray ordenado composto
                      por elementos maiores que `pivot`. Isto forma um subarray
                      ordenado. Q.E.D.


Particionamento não-_in-place_
------------------------------

* Uma versão não _in-place_ do particionamento pode ser facilmente descrita:
    * Sejam dois arrays B' e B''. Para cada elemento j em A, se j não for a
      posição pivô:
        * Se A[j] ≤ A[pivot], concatenar A[j] a B'.
        * Se A[j] > A[pivot], concatenar A[j] a B''.

* Em código:
```
partition-nip(A, s, e, pivot):
    B', B'' = [], []
    for j = s to e-1:
        if j != pivot:
            if A[j] ≤ A[pivot]:
                B'.push(A[j])
            else:
                B''.push(A[j])

    return B', B''
```

* B' e B'' seguem a definição diretamente, logo estão corretos por construção.


Quick Sort _in-place_
---------------------

* As grandes vantagens do Quick Sort, porém, estão na possibilidade de
  implementá-lo com constantes pequenas e espaço auxiliar constante.
  A implementação ingênua vista anteriormente utiliza uma quantidade muito
  maior de espaço auxiliar do que o necessário.

* É possível implementar o passo de partição de forma _in-place_, ou seja,
  no mesmo espaço de memória dado como entrada. Para a função `quicksort`
  a principal mudança é que não haverá retorno - o subarray dado como entrada
  ao final estará ordenado.

* No geral, é considerado uma má prática em programação utilizar algoritmos
  _in-place_ indiscriminadamente, alterando a entrada, mas o algoritmo
  _in-place_ pode ser transformado em não-_in-place_ simplesmente criando
  uma cópia da entrada e rodando o algoritmo _in-place_ sobre a cópia, o
  que ainda utiliza menos memória auxiliar que a solução ingênua dada
  anteriormente (e apenas o necessário de qualquer forma, já que a cópia
  é desejada).

* O código da rotina `quicksort` _in-place_ não é muito diferente:
```pseudo
 1. quicksort(A, s, e):
 2.     if e - s < 2:
 3.         return
 4.
 5.     pivot = choose-pivot(A, s, e)
 6.
 7.     p = partition(A, s, e, pivot)
 8.
 9.     quicksort(A, s, p)
10.     quicksort(A, p+1, e)
```

* A prova, claramente, é similar à prova anterior.

* Notavelmente, os `return`s foram removidos e não é necessario reintroduzir o
  pivô (ele é movido para o lugar pela rotina de partição).

Particionamento _in-place_
--------------------------

* Ingenuamente, é possível fazer o particionamento _in-place_ de forma muito
  simples:
    1. Executar o particionamento não-_in-place_.
    2. Sobrescrever o original com o resultado.

* Obviamente, este não é o resultado esperado, pois o procedimento utilizaria
  espaço auxiliar Θ(n), enquanto já foi afirmado anteriormente que é
  possível utilizar espaço auxiliar constante (Θ(1)).

* O procedimento totalmente _in-place_ ocorre da seguinte forma:
    * Seja o pivô o primeiro elemento no subarray a ser particionado
      (se não for, basta trocá-lo com o primeiro elemento e partir daí).
    * Manter uma divisão no subarray, da seguinte forma:
      ```
      |p| ≤ p | > p |.............|
           visited     not visited
      ```

* Em alto nível, a invariante a manter é:
    * Em uma iteração com j, todos os elementos anteriores a j estão
      particionados.

* Mais detalhadamente:
    * No início de uma iteração com j, seja i um índice tal que A[s+1:i]
      contenha apenas elementos menores ou iguais a A[s] e A[i:j]
      contenha apenas elementos maiores que A[s].

* Em código:
```pseudo
 1. partition(A, s, e, pivot):
 2.     A[s], A[pivot] = A[pivot], A[s]
 3.
 4.     p = A[s]
 5.     i = s+1
 6.
 7.     for j = s+1 to e-1:
 8.         if A[j] ≤ p:
 9.             A[i], A[j] = A[j], A[i]
10.             i += 1
11.
12.     A[s], A[i-1] = A[i-1], A[s]
13.
14.     return i-1
```

* Ou seja, considerando a invariante, ao considerar um novo elemento A[j]:
    * Se A[j] > p, basta avançar j, pois A[i:j+1] continuará sendo formado
      por elementos maiores que p.
    * Se A[j] ≤ p, A[j] é trocado com A[i]. Desta forma, a linha 8 corrige
      a invariante avançando i.

      ```
      Caso 1:
               i    j
      |p| ≤p | >p |[>p]......| -> (incrementando j)

               i        j
      |p| ≤p | >p  [>p]|.....|


      Caso 2:
               i             j
      |p| ≤p |[>p]   >p    |[≤p]......| -> (trocando A[i] com A[j])

               i             j
      |p| ≤p |[≤p]  >p     |[>p]......| -> (incrementando i):

                   i         j
      |p| ≤p  [≤p]|   >p   |[>p]......| -> (incrementando j):

                   i             j
      |p| ≤p  [≤p]|   >p    [>p]|.....|
      ```

* Considerando o argumento para a manutenção da invariante, tem-se:
    * Inicialização: No início do laço i e j são s+1, logo os subarrays
                     são vazios e estão trivialmente corretos.
    * Término: Ao final, é possível trocar o último elemento da primeira
               partição (A[i-1]) com o pivô, reposicionando o pivô para
               além da primeira partição em tempo constante e sem quebrar
               a propriedade de que os elementos são menores ou iguais ao
               pivô.

Análise inicial de complexidade
-------------------------------

* Inicialmente, é confusa a análise de tempo de execução do Quick Sort, pela
  seguinte razão: os subproblemas não possuem um tamanho bem definido. Ao
  escolher um pivô, o subarray pode tão bem ter sido dividido exatamente ao
  meio quanto em uma parte composta por apenas um elemento e todos os
  restantes no lado oposto do pivô.

* Esta característica do Quick Sort impossibilita o uso do _Master Method_
  diretamente. A recorrência possui duas recursões em subarrays de tamanho
  desconhecido (mas garantidamente menor que n) e um passo linear:
    * T(?) + T(??) + Θ(n)

* O detalhe decisivo no tempo de execução então serão os pivôs escolhidos
  durante a execução do algoritmo, como é possível ver a seguir.

* O fato de que as recursões são sempre em subarrays de tamanho menor que n
  facilita reconhecer o melhor e o pior caso:
    * No melhor caso, todas as divisões dos subarrays são da forma mais
      equilibrada possível, ou seja, exatamente no meio do subarray atual.
      Isto ocorre quando a mediana dos elementos no subarray atual é
      utilizada como pivô em todos os casos.
    * No pior caso, todas as divisões diminuem o subproblema em apenas 1,
      ou seja, um dos subarrays fica vazio e todos os elementos que não o
      pivô acabam em uma só partição. Isso ocorre quando o maior ou menor
      elemento do subarray é escolhido como pivô em todos os casos.

* O melhor caso é facilmente analisável: o _Master Method_ passa a ser
  aplicável, pois se todas as divisões são equilibradas, as recursões
  são em subarrays de tamanho Θ(n/2). Logo a recorrência pode ser
  representada por:
    * T(n) = 2\*T(n/2) + Θ(n)

* A recorrência no melhor caso é a mesma de diversos algoritmos já vistos,
  e se resolve para Θ(n\*log(n)).

* No pior caso, uma árvore de recursão auxilia a análise:
```

                 T(n)
                 /  \
               T(0) T(n-1)
                     /  \
                   T(0) T(n-2)
                         /  \
                       T(0) T(n-3)
                               ...   (another n-2 levels)
                                \
                                T(1)
```

* Serão necessárias n recursões de complexidade Θ(n) para chegar ao
  fim da execução, ou seja, em pior caso o tempo de execução é Θ(n\*n),
  ou Θ(n²).

* Com isso, sabe-se que o tempo de execução do Quick Sort é O(n²) e
  Ω(n\*log(n)). Isso não responde muita coisa, porém: o tempo de
  execução quadrático em pior caso pode ser proibitivo no uso do algoritmo
  em casos realísticos. Conforme será visto, felizmente esse não é o caso:
  para uma escolha de pivôs uniformemente aleatória, o tempo de execução
  esperado é Θ(n\*log(n)).
