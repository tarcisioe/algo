def pivot_start(l, start, _):
    return start


def pivot_end(l, _, end):
    return end-1


def pivot_median(l, start, end):
    mid = start + (end - start - 1)//2
    s, m, e = l[start], l[mid], l[end-1]
    values = {s: start, m: mid, e: end-1}
    return values[sorted((s, m, e))[1]]


def partition(l, s, e, pivot):
    l[s], l[pivot] = l[pivot], l[s]

    p = l[s]
    i = s + 1

    for j in range(s+1, e):
        if l[j] <= p:
            l[i], l[j] = l[j], l[i]
            i += 1

    l[s], l[i-1] = l[i-1], l[s]

    return i-1


choose_pivot = pivot_median


def quicksort(l, s, e):
    if e - s < 2:
        return

    pivot = choose_pivot(l, s, e)

    p = partition(l, s, e, pivot)

    quicksort(l, s, p)
    quicksort(l, p + 1, e)


def read_input(file):
    return [int(line) for line in file]

if __name__ == '__main__':
    import sys

    ints = read_input(open(sys.argv[1]))
    quicksort(ints, 0, len(ints))
    print(ints)
