#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <vector>

template <typename Iterator>
Iterator partition(Iterator start, Iterator end, Iterator pivot)
{
    using std::swap;

    const auto &p = *pivot;
    auto i = start;
    ++i;

    for (auto it = i; it != end; ++it) {
        if (*it <= p) {
            swap(*it, *i);
            ++i;
        }
    }

    --i;

    swap(*start, *i);

    return i;
}


template <typename Iterator>
void quicksort(Iterator start, Iterator end)
{
    if (std::distance(start, end) < 2) {
        return;
    }

    auto pivot = start;

    auto p = partition(start, end, pivot);

    quicksort(start, pivot);
    quicksort(++pivot, end);
}


std::vector<int> readInput(const std::string &filename)
{
    std::ifstream input{filename};
    auto ints = std::vector<int>{};
    auto i = 0;

    while (input >> i) {
        ints.push_back(i);
    }

    return ints;
}

int main()
{

    using namespace std::chrono;

    auto test = readInput("input.txt");

    auto before = high_resolution_clock::now();
    quicksort(begin(test), end(test));
    auto after = high_resolution_clock::now();

    for (auto i: test) {
        std::cout << i << "\n";
    }

    std::cout << duration_cast<milliseconds>(after - before).count() << "\n";

    /*
    auto test = std::vector<int>{6, 8, 4, 2, 3, 5, 9, 11, 34};

    partition(begin(test), end(test), begin(test));
    for (auto i: test) {
        std::cout << i << "\n";
    }
    */
    return 0;
}
