def max_difference_interval(A, n):
    max_difference = None
    max_interval = None
    for i in range(n-2):
        for j in range(i+1, n-1):
            difference = A[j] - A[i]
            if max_difference is None or difference > max_difference:
                max_difference = difference
                max_interval = (i, j)

    return max_difference, max_interval


def max_crossing_subarray(A, s, m, e):
    left_sum = A[m]
    max_left, max_right = m, m
    subtotal = 0
    for i in range(m, s-1, -1):
        subtotal += A[i]
        if subtotal > left_sum:
            left_sum = subtotal
            max_left = i

    right_sum = 0
    subtotal = 0
    for j in range(m + 1, e):
        subtotal += A[j]
        if subtotal > right_sum:
            right_sum = subtotal
            max_right = j

    return left_sum + right_sum, (max_left, max_right+1)


def max_subarray_recursive(A, s, e):
    if e - s == 1:
        return A[s], (s, e)

    m = (s + e)//2

    left = max_subarray_recursive(A, s, m)
    right = max_subarray_recursive(A, m, e)
    cross = max_crossing_subarray(A, s, m, e)

    return max(left, right, cross)


def max_subarray(l, start, end):
    a, b = start, start + 1
    i = start
    s = l[start]
    sj = s

    for j in range(start+1, end):
        if sj < 0:
            sj = 0
            i = j

        sj += l[j]

        if sj > s:
            a, b = i, j+1
            s = sj

    return s, (a, b)


# Some test cases:
l = [13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7]
print(max_subarray(l, 0, len(l)))
print(max_subarray_recursive(l, 0, len(l)))
l = [-2, -1, -3, -4]
print(max_subarray(l, 0, len(l)))
print(max_subarray_recursive(l, 0, len(l)))
l = [1, 2, 3, 4, 5]
print(max_subarray(l, 0, len(l)))
print(max_subarray_recursive(l, 0, len(l)))
l = [1]
print(max_subarray(l, 0, len(l)))
print(max_subarray_recursive(l, 0, len(l)))
