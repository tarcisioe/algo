Aula 5 - O Problema do Subarray Máximo
======================================

* Nesta aula, será visto um problema que admite diversas soluções em diversas
  classes de complexidade, o problema do subarray máximo.

Exemplo Prático
---------------

* Seja um valor x que varia de acordo com o tempo, tanto positivamente
  quanto negativamente. Um exemplo seria: a quantidade de horas em um
  banco de horas, a quantidade de dinheiro em uma conta corrente,
  etc.

* A pergunta a ser respondida é: em que intervalo houve o maior
  crescimento do valor de x?

* Perceba que a variação de x precisa envolver casos em que x diminui
  (a variação é negativa), ou a resposta é sempre trivialmente o
  intervalo inteiro.

![Exemplo 1](example1.png)

* Os pontos da imagem são: 3, 0, 4, 3, 6, 2. O intervalo de maior
  crescimento é [1, 4], com uma diferença de 6.

* De início, uma ideia possível é gerar todas as combinações possíveis
  entre pontos, calcular todas as diferenças e retornar a maior.
  Isso está trivialmente correto, pois é uma busca exaustiva
  exatamente pelo resultado procurado.

```pseudo
max-difference-interval(A, n)
    max-difference = none
    max-interval = none
    for i = 0 to n-2:
        for j = i+1 to n-1:
            difference = A[j] - A[i]
            if max-difference is none or difference > max-difference:
                max-difference = difference
                max-interval = (i, j)

    return max-difference, max-interval
```

* Porém, este algoritmo é claramente Θ(n²) em complexidade. Como
  sempre, a questão seguinte é: será possível fazer melhor?

* Uma ideia que soa razoável é procurar os pontos mais alto e mais baixo,
  e então fazer o seguinte: buscar o ponto mais baixo à esquerda do mais
  alto, e o mais alto à direita do mais baixo. Dos dois intervalos, o
  de variação mais positiva é o vencedor. Isto seria um procedimento
  linear, e funciona para o exemplo acima:
    * Ponto mais baixo: 1, de valor 0.
    * Ponto mais alto: 4, de valor 6.
    * Ambas as buscas, para a esquerda a partir do mais alto e para a
      direita a partir do mais baixo empatarão em [1, 4], que pode
      ser dado como resultado.

* Infelizmente, a ideia anterior nem sempre é razoável. Se forem adicionados
  pontos de forma que a sequência seja 6, 7, 3, 0, 4, 3, 6, 2, -1, 1

![Exemplo 2](example2.png)

* Neste caso, o resultado seria o seguinte:
    * Ponto mais baixo: 8, de valor -1
    * Ponto mais alto: 1, de valor 7.
    * A busca para a esquerda a partir do mais alto encontrará o intervalo
      [0, 1] e a busca para a direita a partir do mais baixo encontrará
      o intervalo [8, 9], errando completamente a resposta correta,
      [3, 6].


Reduzindo o Problema
--------------------

* É possível encarar o problema de uma forma diferente: dada a sequência
  de valores, é possível descobrir a sequência de variações, ou seja,
  o quanto o valor variou entre um ponto x e x+1. Por exemplo, seja
  a sequência do segundo exemplo:
    * 6, 7, 3, 0, 4, 3, 6, 2, -1, 1

* A sequência de variações é dada por A[x+1]-A[x] pra cada x até o
  penúltimo elemento:
    * 1, -4, -3, 4, -1, 3, -4, -3, 2

* Cada elemento i na sequência de variações representa a diferença entre
  os elementos i e i+1 na sequência original, e a soma dos valores no
  intervalo [i, j] na sequência de variações representa a diferença entre
  os elementos i e j+1 na sequência original.

* Define-se então o conceito de subarray máximo: uma subsequência
  contígua em um array cuja soma dos valores é a maior possível.
  O subarray máximo na sequência de variações é a maior variação
  que ocorre: justamente o que se estava procurando.

* O problema agora então passa a ser encontrar o subarray máximo na
  sequência de variações. Mas isso não é exatamente uma novidade
  muito útil até agora, pois nenhuma solução foi apresentada.

* Uma solução para esse problema, por mais irrelevante que possa
  parecer, seria: Começando em zero, gerar uma sequência que varie
  pela sequência de variações, e em seguida aplicar o algoritmo
  visto anteriormente. Gerar a sequência seria um processo linear,
  seguido por um processo quadrático. Desta forma, a complexidade
  total seria quadrática. Desta forma sabe-se que é possível
  resolver o problema em O(n²).


Uma Solução por Dividir-e-Conquistar
------------------------------------

* Uma ferramenta já vista anteriormente e que pode ser aplicada
  ao problema do subarray máximo é a ideia do _divide and conquer_,
  a mesma estratégia da busca binária e do Merge Sort.

* A primeira ideia possível é a seguinte: dividir o array na metade,
  de forma a conseguir a divisão mais equilibrada possível, buscar
  recursivamente o subarray máximo em cada uma das metades, comparar
  os resultados e retornar o maior. Isto cobriria os casos em que o
  subarray máximo está inteiro na primeira ou na segunda metade do
  array original:
  ```
         1st             2nd
  |_______________|_______________|
       search            search
         \                 /
           compare results
                 |
               output
  ```

* Como é possível notar facilmente, isto pula um caso em específico:
  o caso em que o subarray máximo cruza a metade do array em que
  se está buscando.

* Porém, é possível encontrar o subarray máximo que cruza a metade
  do array em tempo linear: basta, a partir do ponto médio, iterar
  pelo array tanto para trás quanto para frente, sempre guardando
  o ponto em que o subarray foi máximo.

```pseudo
max-crossing-subarray(A, s, m, e)
    left-sum = A[m]
    max-left, max-right = m, m
    sum = 0
    for i = m downto s:
        sum += A[i]
        if sum > left-sum:
            left-sum = sum
            max-left = i

    right-sum = 0
    sum = 0
    for j = m + 1 to e-1:
        sum += A[j]
        if sum > right-sum:
            right-sum = sum:
            max-right = j

    return left-sum + right-sum, (max-left, max-right+1)
```

* A corretude do procedimento pode ser observada via duas demonstrações
  por manutenção de invariante e um argumento de que dadas as duas
  invariantes mantidas, o resultado é o maior subarray que passe por A[m].

* Com este algoritmo em mãos, podemos cobrir as três únicas possibilidades.
  O subarray máximo precisa necessariamente:
    * Estar na primeira metade do array, ou
    * Estar na segunda metade do array, ou
    * Cruzar o ponto do meio.

* Unindo as três possibilidades em um algoritmo _divide and conquer_:

```pseudo
 1. max-subarray-recursive(A, s, e):
 2.     if e - s == 1:
 3.         return A[s], (s, e)
 4.
 5.     m = (s + e)/2
 6.
 7.     left = max-subarray-recursive(A, s, m)
 8.     right = max-subarray-recursive(A, m, e)
 9.     cross = max-crossing-subarray(A, s, m, e)
10.
11.     return max(left, right, cross)
```

* A corretude do algoritmo pode ser demonstrada via indução:
    * Passo base: Para um array de tamanho 1, o algoritmo retorna
                  o valor do único elemento e seu índice.

    * Hipótese indutiva: Dado um inteiro positivo n, `max-subarray-recursive`
                         está correto para qualquer array de tamanho k &lt; n.

    * Passo indutivo: Seja um subaarray A[s:e] de tamanho n > 1. Sejam três
                      possibilidades:
        * O subarray máximo de A se encontra em A[s:m]. Neste caso, A[s:m]
          possui tamanho &lt; n, logo `max-subarray-recursive` funciona
          corretamente e o encontra na linha 7.
        * O subarray máximo de A se encontra em A[m:e]. Neste caso, A[m:e]
          possui tamanho &lt; n, logo `max-subarray-recursive` funciona
          corretamente e o encontra na linha 8.
        * O subarray máximo de A cruza A[m]. Neste caso,
          `max-crossing-subarray` o encontra na linha 9.

      A linha 11 retorna o máximo dentre os resultados encontrados,
      encarregando-se de selecionar o resultado correto. Logo, dada a
      hipótese indutiva, `max-subarray-recursive` está correto. Q.E.D.

* A complexidade do algoritmo pode ser obtida pelo _Master Method_:
    * O algoritmo é chamado recursivamente duas vezes sobre entradas de
      tamanho n/2.
    * Além das chamadas recursivas, é executado trabalho Θ(n) na linha
      9 e trabalho constante em todas as outras.
    * Desta forma, a recorrência que descreve o tempo de execução é:
        * T(n) = 2T(n/2) + Θ(n)
    * Pelo _Master Method_:
        * a = 2
        * b = 2
        * d = 1
        * Temos a = b^d, logo T(n) = Θ(n^(d)\*log(n)), ou seja,
          Θ(n\*log(n)).
        * A recorrência é a mesma de `merge-sort`, e o resultado é
          consistente.


Uma Solução Linear
------------------

* Na realidade, o problema do subarray máximo é solúvel em tempo linear.
  Esta solução, claramente, é assintóticamente ótima, pois como nenhuma
  restrição é imposta no array dado como entrada, é necessário no mínimo
  examinar cada item.

* A base da solução é a seguinte afirmação: seja um subarray A[s:j]
  qualquer. Um subarray máximo A' de A[s:j+1] cai necessariamente em um
  de dois casos:
    1. A' = A[i:j+1], com s ≤ i &lt; j+1, ou
    2. A' é um subarray máximo de A[s:j].

* Desta forma, iterando-se sobre o array é possível manter registro
  sobre:
    * O subarray máximo visto até agora, e
    * O subarray de maior soma terminado em j.

* Se as invariantes dos registros puderem ser mantidas em tempo constante,
  o algoritmo final será linear.

* Em uma iteração com j qualquer do algoritmo, supõe-se válidos:
    * a e b, representando os limites [a, b) do subarray máximo de A[0:j].
    * i, o índice inicial do maior subarray terminado em A[j].
    * s e sj, o valor da soma dos itens de A[a:b] e de A[i:j], respectivamente.

     ```
                      A[i:j]
                     /      \ j
     |                       | | ... |   Nota: A notação A[i:j] é exclusiva em
        \__________/                           j.
           A[a:b]
     ```

* Primeiramente é importante notar que A[i:j] necessariamente ter seu limite
  final deslocado em 1 para terminar sempre em j. Assim sendo, a manutenção
  necessária é em i.

* A princípio, i poderia ser reposicionado para qualquer posição entre i+1 e
  j. Buscar esta posição seria potencialmente linear, tornando o algoritmo
  todo quadrático. Porém, com uma análise mais profunda, é possível notar
  que apenas duas possibilidades fazem sentido:
    * Ou A[j] é, sozinho, o maior subarray até j, logo i = j, ou
    * A[j] é incorporado a A[i:j] por algum motivo.

* Para visualizar o motivo de ser impossível que i seja reposicionado para
  algum valor &lt; j, é importante lembrar que A[i:j] é o subarray de maior
  soma terminado em j-1. Seja i' tal que i &lt; i' &lt; j. Logo, reposicionar
  i para i' removeria o prefixo A[i:i'] do array. A soma de A[i:i'] poderia
  ser, a princípio, negativa, igual a zero ou positiva. Sejam os três casos:
    * A[i:i'] soma positivo. Neste caso, descontar a soma do prefixo diminuiria
      a soma total, e não é vantajoso reposicionar i.
    * A[i:i'] soma zero. Neste caso é irrelevante descontar a soma do prefixo,
      pois ela não altera a soma total.
    * A[i:i'] soma negativo. Na realidade este caso é impossível, pois sendo
      a soma de A[i:i'] si', sj - si' > sj, e A[i:j] não seria o subarray
      de maior soma terminado em j-1, o que forma uma contradição.

* Desta forma, apenas dois valores fazem sentido para i ao considerar um
  elemento A[j] para participar de A[i:j+1]: i ou j. Para tomar essa
  decisão é necessário considerar a soma atual, sj.

* Sabe-se que em toda iteração A[j] será inevitavelmente somado a sj,
  pois sj sempre se refere ao subarray de maior soma terminado em j.
  Desta forma, o laço terá a seguinte forma:
    ```pseudo
    for j = start+1 to end-1:
        if CONDITION:
            sj = 0
            i = j

        sj += A[j]

        if sj > s:
            a, b = i, j+1
            s = sj
    ```

* Resta definir a condição do `if`. Espera-se maximizar sj. Logo,
  a condição para reiniciar o array de maior soma atual deve
  ser `sj < 0`. A explicação é a seguinte:
    * Se sj for positivo, para qualquer valor de A[j], sj + A[j] > A[j],
      ou seja, é vantajoso manter o prefixo A[i:j].
    * Se sj for zero, é irrelevante manter ou descartar A[s:j].
    * Se sj for negativo, sj + A[j] < A[j] para qualquer valor de A[j],
      ou seja, é vantajoso descartar o prefixo A[i:j].

* Por fim, o algoritmo então será:

```pseudo
max-subarray-linear(A, s, e):
    a, b = start, start+1
    i = start
    sj = A[start]
    s = A[start]

    for j = start+1 to end-1:
        if sj < 0:
            sj = 0
            i = j

        sj += A[j]

        if sj > s:
            a, b = i, j+1
            s = sj

    return s, (a, b)
```

* O algoritmo é facilmente reconhecível como linear, pois faz n-1
  iterações compostas somente por operações de tempo constante.
  Logo, seu tempo de execução é Θ(n).

* A corretude do algoritmo pode ser verificada demonstrando a
  manutenção das invariantes já citadas.
